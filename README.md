# Welcome to `spade-api` #

## Overview ##

[SPADE - API](https://gitlab.com/nest.lbl.gov/spade-api) contains the interfaces that can be used to customize the behaviour of [SPADE](https://gitlab.com/nest.lbl.gov/spade).

In order to build this project see the [developer notes](developer_notes.md).
