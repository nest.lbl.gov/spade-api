package gov.lbl.nest.spade.registry;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class provides access to the drop box information of a file stream.
 * 
 * @author patton
 * 
 */
@XmlType(propOrder = { "neighborEmail",
                       "location",
                       "pattern",
                       "mapping",
                       "subdirs"})
public class DropBox {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link ExternalLocation} that will be searched for files.
     */
    private ExternalLocation location;

    /**
     * The {@link DropMapping} instance used to map the semaphore file to the
     * data file.
     */
    private DropMapping mapping;

    /**
     * The email address of the neighbor, if any, that delivers files to this
     * drop box.
     */
    private String neighborEmail;

    /**
     * The {@link DropPattern} that will be used to match files in this object's
     * location.
     */
    private DropPattern pattern;

    /**
     * <code>false</code> if sub-directories of the {@link #location} should
     * <em>not</em> be searched.
     */
    private Boolean subdirs;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected DropBox() {
    }

    // instance member method (alphabetic)

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DropBox other = (DropBox) obj;
        if (mapping == null) {
            if (other.mapping != null)
                return false;
        } else if (!mapping.equals(other.mapping))
            return false;
        if (location == null) {
            if (other.location != null)
                return false;
        } else if (!location.equals(other.location))
            return false;
        if (neighborEmail == null) {
            if (other.neighborEmail != null)
                return false;
        } else if (!neighborEmail.equals(other.neighborEmail))
            return false;
        if (pattern == null) {
            if (other.pattern != null)
                return false;
        } else if (!pattern.equals(other.pattern))
            return false;
        return true;
    }

    /**
     * Returns the {@link ExternalLocation} that will be searched for files.
     * 
     * @return the {@link ExternalLocation} that will be searched for files.
     */
    @XmlElement(required = true)
    public ExternalLocation getLocation() {
        return location;
    }

    /**
     * Returns the {@link DropMapping} instance used to map the semaphore file
     * to the data file.
     * 
     * @return the {@link DropMapping} instance used to map the semaphore file
     *         to the data file.
     */
    @XmlElement
    public DropMapping getMapping() {
        return mapping;
    }

    /**
     * Returns the email address of the neighbor, if any, that delivers files to
     * this drop box.
     * 
     * @return the email address of the neighbor, if any, that delivers files to
     *         this drop box.
     */
    @XmlElement(name = "neighbor")
    public String getNeighborEmail() {
        return neighborEmail;
    }

    /**
     * Returns the {@link DropPattern} that will be used to match files in this
     * object's location.
     * 
     * @return the {@link DropPattern} that will be used to match files in this
     *         object's location.
     */
    @XmlElement(required = true)
    public DropPattern getPattern() {
        return pattern;
    }

    /**
     * Returns <code>false</code> if sub-directories of the {@link #location}
     * should <em>not</em> be searched.
     * 
     * @return <code>false</code> if sub-directories of the {@link #location}
     *         should <em>not</em> be searched.
     */
    @XmlElement
    public Boolean getSubdirs() {
        return subdirs;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        // TODO Auto-generated method stub
        return super.clone();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((mapping == null) ? 0
                                                     : mapping.hashCode());
        result = prime * result + ((location == null) ? 0
                                                      : location.hashCode());
        result = prime * result + ((neighborEmail == null) ? 0
                                                           : neighborEmail.hashCode());
        result = prime * result + ((pattern == null) ? 0
                                                     : pattern.hashCode());
        return result;
    }

    /**
     * Sets the {@link ExternalLocation} that will be searched for files.
     * 
     * @param location
     *            the {@link ExternalLocation} that will be searched for files.
     */
    protected void setLocation(ExternalLocation location) {
        this.location = location;
    }

    /**
     * Sets the {@link DropMapping} instance used to map the semaphore file to
     * the data file.
     * 
     * @param mapping
     *            the {@link DropMapping} instance used to map the semaphore
     *            file to the data file.
     */
    protected void setMapping(DropMapping mapping) {
        this.mapping = mapping;
    }

    /**
     * Sets the email address of the neighbor, if any, that delivers files to
     * this drop box.
     * 
     * @param email
     *            the email address of the neighbor, if any, that delivers files
     *            to this drop box.
     */
    protected void setNeighborEmail(String email) {
        neighborEmail = email;
    }

    /**
     * Sets the {@link DropPattern} that will be used to match files in this
     * object's location.
     * 
     * @param pattern
     *            the {@link DropPattern} that will be used to match files in
     *            this object's location.
     */
    protected void setPattern(DropPattern pattern) {
        this.pattern = pattern;
    }

    /**
     * Sets whether the sub-directories of the {@link #location} should
     * <em>not</em> be searched or not
     * 
     * @param subdirs
     *            <code>false</code> if sub-directories of the {@link #location}
     *            should <em>not</em> be searched.
     */
    protected void setSubdirs(Boolean subdirs) {
        this.subdirs = subdirs;
    }

    // static member methods (alphabetic)

    // Description of this object.
    @Override
    public String toString() {
        return location.toString() + "/"
               + pattern.toString();
    }

    // public static void main(String args[]) {}
}
