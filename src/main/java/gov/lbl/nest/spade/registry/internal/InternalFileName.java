package gov.lbl.nest.spade.registry.internal;

/**
 * This class validates and provides access to the components of an internal
 * file name.
 * 
 * @author patton
 */
public class InternalFileName {

    // public static final member data

    /**
     * The directory where the payload of data files is held.
     */
    public static final String PAYLOAD_DIRECTORY = "payload";

    /**
     * The suffix to use for temporary files.
     */
    public static final String TMP_SUFFIX = ".tmp";

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The suffix use on metadata files
     */
    private static final String METADATA_SUFFIX = ".meta.xml";

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    /**
     * Return the name of the metadata filename for the bundle with the supplied
     * name.
     * 
     * @param name
     *            the name of the bundle whose metadata filename should be
     *            returned.
     * @return the name of the metadata filename for the bundle with the
     *         supplied name.
     */
    public static String getMetadataFilename(final String name) {
        if (null == name) {
            return null;
        }
        return name + METADATA_SUFFIX;
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
