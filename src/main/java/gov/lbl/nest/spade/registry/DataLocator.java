package gov.lbl.nest.spade.registry;

import java.io.File;
import java.util.List;

/**
 * This interface is used to encapsulate a semaphore-data-files mapping.
 * 
 * @author patton
 */
public interface DataLocator {

    /**
     * Returns the {@link ExternalFile} instances for one or more data files
     * based on the {@link ExternalLocation} of where the external semaphore
     * file was found and the local copy of that file.
     * 
     * @param externalSemaphore
     *            the {@link ExternalLocation} of where the external semaphore
     *            file was found
     * @param internalSemaphore
     *            TODO
     * @param identity
     *            TODO
     * @return the {@link ExternalFile} instances for one or more data files.
     */
    List<ExternalFile> locateData(final ExternalFile externalSemaphore,
                                  final File internalSemaphore,
                                  final String identity);

    /**
     * Returns the name of the bundle associated with the supplied semaphore
     * file.
     * 
     * @param semaphoreName
     *            the semaphore file of the bundle.
     * @return the name of the bundle associated with the supplied semaphore
     *         file.
     */
    String getBundleName(final String semaphoreName);

}
