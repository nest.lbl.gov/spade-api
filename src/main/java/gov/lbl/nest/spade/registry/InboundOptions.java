package gov.lbl.nest.spade.registry;

import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to contain the setting of available ingestion options for
 * an inbound Bundle.
 * 
 * @author patton
 */
@XmlType
public class InboundOptions extends
                            Options {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    /**
     * Creates an instance of this class.
     */
    public InboundOptions() {
        // Required for JAXB
    }

    /**
     * Creates an instance of this class
     * 
     * @param rhs
     *            the {@link InboundOptions} instance from which to make this
     *            object.
     */
    private InboundOptions(final InboundOptions rhs) {
        super(rhs);
    }

    // instance member method (alphabetic)

    @Override
    public boolean cacheData() {
        return true;
    }

    @Override
    public boolean createMetadata() {
        return false;
    }

    @Override
    public Options copy() {
        return new InboundOptions(this);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
