package gov.lbl.nest.spade.registry;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementWrapper;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

import gov.lbl.nest.common.rs.Resource;
import gov.lbl.nest.spade.registry.internal.InboundRegistration;

/**
 * This class is a collection of known registrations within the current
 * application.
 * 
 * @author patton
 */
@XmlRootElement(name = "registry")
@XmlType(propOrder = { "local",
                       "inbound" })
public class Registry extends
                      Resource {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The collection of {@link InboundRegistration} instances in this object.
     */
    List<InboundRegistration> inbound = new ArrayList<InboundRegistration>();

    /**
     * The collection of {@link LocalRegistration} instances in this object.
     */
    List<LocalRegistration> local = new ArrayList<LocalRegistration>();

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Registry() {
        // Do nothing
    }

    /**
     * Creates an instance of this class.
     * 
     * @param local
     *            the collection of {@link LocalRegistration} instances in this
     *            object.
     * @param inbound
     *            the collection of {@link InboundRegistration} instances in this
     *            object.
     */
    public Registry(final List<LocalRegistration> local,
                    final List<InboundRegistration> inbound) {
        setInbound(inbound);
        setLocal(local);
    }

    // instance member method (alphabetic)

    /**
     * Returns the collection of {@link InboundRegistration} instances in this
     * object.
     * 
     * @return the collection of {@link InboundRegistration} instances in this
     *         object.
     */
    @XmlElement(name = "registration")
    @XmlElementWrapper(name = "inbound")
    public List<InboundRegistration> getInbound() {
        return inbound;
    }

    /**
     * Returns the collection of {@link LocalRegistration} instances in this object.
     * 
     * @return the collection of {@link LocalRegistration} instances in this object.
     */
    @XmlElement(name = "registration")
    @XmlElementWrapper(name = "local")
    public List<LocalRegistration> getLocal() {
        return local;
    }

    /**
     * Sets the collection of {@link InboundRegistration} instances in this object.
     * 
     * @param registrations
     *            the collection of {@link InboundRegistration} instances in this
     *            object.
     */
    protected void setInbound(List<InboundRegistration> registrations) {
        inbound = registrations;
    }

    /**
     * Sets the collection of {@link LocalRegistration} instances in this object.
     * 
     * @param registrations
     *            the collection of {@link LocalRegistration} instances in this
     *            object.
     */
    protected void setLocal(List<LocalRegistration> registrations) {
        local = registrations;
    }

    /**
     * Sets the URI of this resource.
     * 
     * @param uri
     *            the URI of this resource.
     */
    @Override
    public void setUri(URI uri) {
        super.setUri(uri);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
