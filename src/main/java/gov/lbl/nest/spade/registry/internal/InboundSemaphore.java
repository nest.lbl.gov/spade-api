package gov.lbl.nest.spade.registry.internal;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import gov.lbl.nest.spade.registry.FileCategory;

/**
 * This class is used to interpret the name of an inbound semaphore file.
 * 
 * @author patton
 */
public class InboundSemaphore {

    // public static final member data

    /**
     * The {@link Pattern} instance used to parse the semaphore's name.
     */
    public static final Pattern PATTERN = getRegistrationPattern("[^" + InboundLocator.SEPARATOR
                                                                 + "]*");
    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The regex index of the format version of the inbound semaphore file.
     */
    private static final int VERSION_INDEX = 1;

    /**
     * The regex index of the remote ticket value.
     */
    private static final int TICKET_INDEX = VERSION_INDEX + 1;

    /**
     * The regex index of the remote registration value.
     */
    private static final int FILECATEGORY_INDEX = TICKET_INDEX + 1;

    /**
     * The regex index of the remote registration value.
     */
    private static final int REGISTRATION_INDEX = FILECATEGORY_INDEX + 1;

    /**
     * The regex index of the bundle name.
     */
    private static final int BUNDLE_INDEX = REGISTRATION_INDEX + 1;

    // private static member data

    // private instance member data

    /**
     * The bundle name for the inbound semaphore file.
     */
    final String bundle;

    /**
     * The registration within the delivering SPADE.
     */
    final String registration;

    /**
     * The ticket identity within the delivering SPADE.
     */
    final String ticket;

    /**
     * The format version of the inbound semaphore file.
     */
    final String version;

    // constructors

    /**
     * Creates an instance of this class.
     * 
     * @param name
     *            the name of the inbound semaphote file.
     */
    public InboundSemaphore(final String name) {
        final Matcher matcher = PATTERN.matcher(name);
        if (!matcher.find()) {
            throw new IllegalArgumentException("\"" + name
                                               + "\" is not a valid inbound semaphore name");
        }
        bundle = matcher.group(BUNDLE_INDEX);
        registration = matcher.group(REGISTRATION_INDEX);
        ticket = matcher.group(TICKET_INDEX);
        version = matcher.group(VERSION_INDEX);
    }

    // instance member method (alphabetic)

    /**
     * Returns the registration within the delivering SPADE.
     * 
     * @return the registration within the delivering SPADE.
     */
    public String getRegistration() {
        return registration;
    }

    /**
     * Returns the ticket identity within the delivering SPADE.
     * 
     * @return the ticket identity within the delivering SPADE.
     */
    public String getTicket() {
        return ticket;
    }

    // static member methods (alphabetic)

    /**
     * Returns the {@link Pattern} instance valid for the supplied
     * {@link InboundRegistration} instance name.
     * 
     * @param name
     *            the name of the {@link InboundRegistration} instance whose
     *            {@link Pattern} should be returned.
     * @return the {@link Pattern} instance valid for the supplied
     *         {@link InboundRegistration} instance name.
     */
    public static Pattern getRegistrationPattern(final String name) {
        return Pattern.compile("(" + InboundLocator.V4_PREFIX
                               + ")"
                               + InboundLocator.SEPARATOR
                               + "([^"
                               + InboundLocator.SEPARATOR
                               + "]*)"
                               + InboundLocator.SEPARATOR
                               + "("
                               + FileCategory.DONE
                               + ")"
                               + InboundLocator.SEPARATOR
                               + "("
                               + name
                               + ")"
                               + InboundLocator.SEPARATOR
                               + "(.*)$");
    }

    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
