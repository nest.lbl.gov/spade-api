package gov.lbl.nest.spade.registry.internal;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;

import gov.lbl.nest.spade.registry.DataLocator;
import gov.lbl.nest.spade.registry.ExternalLocation;
import gov.lbl.nest.spade.registry.FileCategory;
import gov.lbl.nest.spade.registry.HandlingPriority;
import gov.lbl.nest.spade.registry.InboundOptions;
import gov.lbl.nest.spade.registry.InboundTransferRef;
import gov.lbl.nest.spade.registry.Registration;

/**
 * This class is used to define how an inbound (as opposed to local) bundle
 * should be managed and also how find new ones.
 * 
 * @author patton
 */
@XmlRootElement(name = "registration")
@XmlType(propOrder = { "localId",
                       "priority",
                       "inboundReference",
                       "scanLimit",
                       "analyze",
                       "archive",
                       "duplications",
                       "warehouse",
                       "outboundTransfers" })
public class InboundRegistration extends
                                 Registration {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link InboundLocator} used by all instances of this class.
     */
    private static final InboundLocator INBOUND_LOCATOR = new InboundLocator();

    // private static member data

    // private instance member data

    /**
     * The {@link InboundOptions} instance used by this class.
     */
    // private InboundOptions inboundOptions;

    /**
     * The reference to the inbound transfer associated with this registration.
     */
    private InboundTransferRef inboundReference;

    /**
     * The name of the neighboring SPADE deployment delivering to this object.
     */
    private String neighbor;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected InboundRegistration() {
        this(new InboundOptions());
    }

    /**
     * Creates an instance of this class.
     * 
     * @param options
     *            the {@link InboundOptions} instance used by this class.
     */
    private InboundRegistration(InboundOptions options) {
        super(options);
        // inboundOptions = options;
    }

    /**
     * Creates an instance of this class.
     * 
     * @param localId
     *            the unique identity of this object within the set of local
     *            {@link Registration} instances.
     * @param neighborName
     *            the name of the neighboring SPADE deployment that delivers
     *            files using this object.
     * @param transfer
     *            the {@link InboundTransferRef} instance that refers to the
     *            inbound transfer that delivers files using this object.
     * @param dropLocation
     *            the directory assigned to be the DropBox area for this
     *            registration.
     */
    public InboundRegistration(final String localId,
                               final String neighborName,
                               final InboundTransferRef transfer,
                               final ExternalLocation dropLocation) {
        this();
        setLocalId(localId);
        bind(neighborName,
             transfer,
             dropLocation,
             InboundSemaphore.PATTERN);
    }

    // instance member method (alphabetic)

    /**
     * 
     * @param neighborName
     *            the name of the neighboring SPADE deployment that delivers
     *            files using this object.
     * @param transfer
     *            the {@link InboundTransferRef} instance that refers to the
     *            inbound transfer that delivers files using this object.
     * @param dropLocation
     *            the directory assigned to be the DropBox area for this
     *            registration.
     * @param pattern
     *            the {@link Pattern} instance that must be matched by a file
     *            name in order for it to match this registration.
     */
    public void bind(final String neighborName,
                     final InboundTransferRef transfer,
                     final ExternalLocation dropLocation,
                     final Pattern pattern) {
        setDropLocation(dropLocation);
        setInboundReference(transfer);
        neighbor = neighborName;
        setPattern(pattern);
    }

    @Override
    @XmlElement(defaultValue = "true")
    public Boolean getAnalyze() {
        return super.getAnalyze();
    }

    @Override
    @XmlElement(defaultValue = "DATA")
    public FileCategory getArchive() {
        return super.getArchive();
    }

    @Override
    @XmlTransient
    public DataLocator getDataLocator() {
        return INBOUND_LOCATOR;
    }

    @Override
    @XmlElement(name = "duplication",
                defaultValue = ".default.")
    public Collection<String> getDuplications() {
        return super.getDuplications();
    }

    /**
     * Returns the reference to the inbound transfer associated with this
     * registration.
     * 
     * @return the reference to the inbound transfer associated with this
     *         registration.
     */
    @XmlElement(name = "inbound_transfer")
    public InboundTransferRef getInboundReference() {
        return inboundReference;
    }

    @Override
    @XmlElement(name = "local_id")
    public String getLocalId() {
        return super.getLocalId();
    }

    @Override
    @XmlTransient
    public String getNeighbor() {
        return neighbor;
    }

    @Override
    @XmlElement(name = "outbound_transfer")
    public Collection<String> getOutboundTransfers() {
        return super.getOutboundTransfers();
    }

    @Override
    @XmlElement(defaultValue = "NORMAL")
    public HandlingPriority getPriority() {
        return super.getPriority();
    }

    @Override
    @XmlTransient
    public String getRemoteId() {
        return inboundReference.getRemoteId();
    }

    @Override
    @XmlElement(name = "scan_limit")
    public Integer getScanLimit() {
        return super.getScanLimit();
    }

    @Override
    @XmlElement(defaultValue = "true")
    public Boolean getWarehouse() {
        return super.getWarehouse();
    }

    @Override
    public Boolean getSubdirs() {
        return Boolean.FALSE;
    }

    @Override
    protected void setAnalyze(final Boolean analyze) {
        super.setAnalyze(analyze);
    }

    @Override
    protected void setArchive(final FileCategory archive) {
        super.setArchive(archive);
    }

    @Override
    protected void setLocalId(final String localId) {
        super.setLocalId(localId);
    }

    @Override
    protected void setDuplications(final Collection<String> duplications) {
        super.setDuplications(duplications);
    }

    /**
     * Sets the reference to the inbound transfer associated with this
     * registration.
     * 
     * @param reference
     *            the reference to the inbound transfer associated with this
     *            registration.
     */
    protected void setInboundReference(final InboundTransferRef reference) {
        this.inboundReference = reference;
    }

    @Override
    protected void setOutboundTransfers(final Collection<String> outboundTransfers) {
        super.setOutboundTransfers(outboundTransfers);
    }

    @Override
    protected void setPriority(final HandlingPriority priority) {
        super.setPriority(priority);
    }

    @Override
    protected void setScanLimit(final Integer limit) {
        super.setScanLimit(limit);
    }

    @Override
    protected void setWarehouse(final Boolean warehouse) {
        super.setWarehouse(warehouse);
    }

    // static member methods (alphabetic)

    /**
     * Returns a {@link InboundRegistration} instance read from an XML
     * representation of it provided by the specified {@link InputStream}.
     * 
     * @param stream
     *            the {@link InputStream} containing the XML representation.
     * 
     * @return the instance of this class read from the {@link InputStream}.
     */
    private static InboundRegistration read(final InputStream stream) {
        try {
            JAXBContext content = JAXBContext.newInstance(InboundRegistration.class);
            final Unmarshaller unmarshaller = content.createUnmarshaller();
            return (InboundRegistration) unmarshaller.unmarshal(stream);
        } catch (JAXBException e1) {
            e1.printStackTrace();
            return null;
        }
    }

    /**
     * Recursively instantiate all {@link InboundRegistration} instances in the
     * specified directory and below.
     * 
     * @param directory
     *            the directory from which to read the
     *            {@link InboundRegistration} instances.
     * 
     * @return the collection of {@link InboundRegistration} instances read.
     */
    public static List<InboundRegistration> readInboundFromTree(final File directory) {
        List<InboundRegistration> result = new ArrayList<InboundRegistration>();
        final File[] files = directory.listFiles();
        if (null != files) {
            for (File file : files) {
                final String name = file.getName();
                if (file.isDirectory()
                    && !(name.startsWith(DISABLED_REGISTRATIONS_FLAG)
                         || name.endsWith(DISABLED_REGISTRATIONS_FLAG))) {
                    result.addAll(readInboundFromTree(file));
                } else {
                    // Only consider XML files.
                    if (name.endsWith(".xml")
                        && !(name.startsWith(DISABLED_REGISTRATIONS_FLAG))) {
                        try {
                            final InboundRegistration registration = read(new FileInputStream(file));
                            if (null != registration) {
                                result.add(registration);
                            }
                        } catch (FileNotFoundException e) {
                            // Should not be able to get here!
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return result;
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
