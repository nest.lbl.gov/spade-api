package gov.lbl.nest.spade.registry;

import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to contain the setting of available options for
 * re-shipping Bundle.
 * 
 * @author patton
 */
@XmlType
public class RetryOptions extends
                          Options {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected RetryOptions() {
        // Required for JAXB
    }

    /**
     * Creates an instance of this class.
     * 
     * @param rhs
     *            the {@link Options} instance from which to make this object.
     */
    public RetryOptions(final Options rhs) {
        setCompress(rhs.getCompress());
        setEmbedded(rhs.getEmbedded());
        setExpand(rhs.getExpand());
        setPack(rhs.getPack());
        setPriority(rhs.getPriority());
        setSingle(rhs.isSingle());
        setUnpack(rhs.getUnpack());
    }

    // instance member method (alphabetic)

    @Override
    public boolean cacheData() {
        return false;
    }

    @Override
    public boolean createMetadata() {
        return false;
    }

    @Override
    public Options copy() {
        return new RetryOptions(this);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
