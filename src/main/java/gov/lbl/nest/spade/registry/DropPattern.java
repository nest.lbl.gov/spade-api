package gov.lbl.nest.spade.registry;

import java.util.regex.Pattern;

import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.XmlValue;

/**
 * This class defines a pattern with which to match semaphore files in a
 * {@link ExternalLocation}.
 * 
 * @author patton
 */
@XmlType()
public class DropPattern {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The regular expression used to match the base part of the semaphore file.
     */
    private String regex;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected DropPattern() {
    }

    // instance member method (alphabetic)

    /**
     * Returns the regular expression used to match the base part of the
     * semaphore file.
     * 
     * @return the regular expression used to match the base part of the
     *         semaphore file.
     */
    @XmlValue
    public String getRegex() {
        return regex;
    }

    Pattern getPattern() {
        return Pattern.compile(getRegex());
    }

    /**
     * Sets the regular expression used to match the base part of the semaphore
     * file.
     * 
     * @param regex
     *            the regular expression used to match the base part of the
     *            semaphore file.
     */
    protected void setRegex(String regex) {
        this.regex = regex;
    }

    // static member methods (alphabetic)

    // Description of this object.
    @Override
    public String toString() {
        return getRegex();
    }

    // public static void main(String args[]) {}
}
