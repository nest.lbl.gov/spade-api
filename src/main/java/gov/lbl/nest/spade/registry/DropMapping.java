package gov.lbl.nest.spade.registry;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import jakarta.xml.bind.annotation.XmlValue;

/**
 * This class defines the class used to map a semaphore file onto its
 * data-files.
 * 
 * @author patton
 */
public class DropMapping {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link DataLocator} instance used by this object to create SPADE
     * Bundle instances.
     */
    private DataLocator dataLocator;

    /**
     * The canonical name of the class to used to locate the data file.
     */
    private String locatorClass;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected DropMapping() {
    }

    // instance member method (alphabetic)

    /**
     * Returns the {@link DataLocator} instance, defined for this instance, that
     * is used to map a semaphore file onto its data-files.
     * 
     * @param warehouseRoot
     *            the {@link File} instance of the local warehouse so bundles
     *            can be transferred out of the warehouse.
     * 
     * @return the {@link DataLocator} instance, defined for this instance, that
     *         is used to map a semaphore file onto its data-files.
     */
    DataLocator getDataLocator(File warehouseRoot) {
        if (null != locatorClass && null == dataLocator) {
            final ClassLoader loader = getClass().getClassLoader();
            try {
                @SuppressWarnings("unchecked")
                final Class<DataLocator> clazz = (Class<DataLocator>) loader.loadClass(locatorClass);
                if (null != warehouseRoot) {
                    try {
                        final Constructor<DataLocator> constructor = clazz.getConstructor(new Class<?>[] { File.class });
                        return constructor.newInstance(warehouseRoot);
                    } catch (NoSuchMethodException e) {
                        // Do nothing, continue and use simple constructor.
                    }
                }
                final Constructor<DataLocator> constructor = clazz.getConstructor();
                dataLocator = constructor.newInstance();
            } catch (NoSuchMethodException e) {
                throw new IllegalArgumentException("No default constructor of class \""
                                                   + locatorClass
                                                   + "\"",
                                                   e);
            } catch (SecurityException e) {
                throw new IllegalArgumentException("Constructor of class \""
                                                   + locatorClass
                                                   + "\" can not be accessed with its Activity as an argument",
                                                   e);
            } catch (ClassNotFoundException e) {
                throw new IllegalArgumentException("No such class as \""
                                                   + locatorClass
                                                   + "\"",
                                                   e);
            } catch (InstantiationException e) {
                throw new IllegalArgumentException("Can not instantiate class \""
                                                   + locatorClass
                                                   + "\"",
                                                   e);
            } catch (IllegalAccessException e) {
                throw new IllegalArgumentException("Constructor of class \""
                                                   + locatorClass
                                                   + "\" can not be accessed",
                                                   e);
            } catch (InvocationTargetException e) {
                throw new IllegalArgumentException("Can not invoke default constructor of class \""
                                                   + locatorClass
                                                   + "\"",
                                                   e);
            }
        }
        return dataLocator;
    }

    /**
     * Returns the canonical name of the class to used to locate the data file.
     * 
     * @return the canonical name of the class to used to locate the data file.
     */
    @XmlValue
    protected String getLocatorClass() {
        return locatorClass;
    }

    /**
     * Sets the canonical name of the class to used to locate the data file.
     * 
     * @param className
     *            the canonical name of the class to used to locate the data
     *            file.
     */
    protected void setLocatorClass(String className) {
        final String classNameToUse;
        if (null != className) {
            classNameToUse = className.trim();
            if (null != locatorClass && null != classNameToUse
                && locatorClass.equals(classNameToUse)) {
                return;
            }
        } else {
            classNameToUse = null;
        }
        dataLocator = null;
        locatorClass = classNameToUse;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
