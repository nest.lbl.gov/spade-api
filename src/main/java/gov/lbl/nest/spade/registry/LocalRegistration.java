package gov.lbl.nest.spade.registry;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;

import gov.lbl.nest.spade.registry.internal.InboundLocator;

/**
 * This class is used to define how a local (as opposed to inbound) bundle
 * should be managed and also how find new ones.
 * 
 * @author patton
 */
@XmlRootElement(name = "registration")
@XmlType(propOrder = { "localId",
                       "priority",
                       "dropBox",
                       "defaultMetadataPath",
                       "scanLimit",
                       "analyze",
                       "archive",
                       "compress",
                       "duplications",
                       "embedded",
                       "owner",
                       "pack",
                       "warehouse",
                       "outboundTransfers" })
public class LocalRegistration extends
                               Registration {

    private static class DefaultLocator extends
                                        SuffixSubstitution {
        public DefaultLocator() {
            super(".sem",
                  ".data");
        }
    }

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link InboundLocator} used by all instances of this class.
     */
    private static final DataLocator DEFAULT_LOCAL_LOCATOR = new DefaultLocator();

    // private static member data

    // private instance member data

    /**
     * The {@link DropBox} instance that determines how new files are found.
     */
    private DropBox dropBox;

    /**
     * The {@link LocalOptions} instance used by this class.
     */
    private LocalOptions localOptions;

    /**
     * The {@link File} instance of the local warehouse so bundles can be
     * transferred out of the warehouse.
     */
    private File warehouseRoot;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected LocalRegistration() {
        this(new LocalOptions());
    }

    /**
     * Creates an instance of this class to test purposes.
     * 
     * @param localId
     *            the unique identity of this object within the set of local
     *            {@link Registration} instances.
     */
    public LocalRegistration(final String localId) {
        super(localId);
    }

    /**
     * Creates an instance of this class.
     * 
     * @param options
     *            the {@link LocalOptions} instance used by this class.
     */
    private LocalRegistration(final LocalOptions options) {
        super(options);
        this.localOptions = options;
    }

    // instance member method (alphabetic)

    @Override
    @XmlElement(defaultValue = "true")
    public Boolean getAnalyze() {
        return super.getAnalyze();
    }

    @Override
    @XmlElement(defaultValue = "DATA")
    public FileCategory getArchive() {
        return super.getArchive();
    }

    @Override
    @XmlElement(defaultValue = "true")
    public Boolean getCompress() {
        return super.getCompress();
    }

    @Override
    @XmlTransient
    public DataLocator getDataLocator() {
        final DropMapping dropMapping = getDropBox().getMapping();
        if (null == dropMapping) {
            return DEFAULT_LOCAL_LOCATOR;
        }
        final DataLocator result = dropMapping.getDataLocator(warehouseRoot);
        return result;
    }

    /**
     * Returns the path, if any, of the file containing the default metadata to
     * use for files matching this registration.
     * 
     * @return the path, if any, of the file containing the default metadata to
     *         use for files matching this registration.
     */
    @XmlElement(name = "default_metadata")
    public String getDefaultMetadataPath() {
        return localOptions.getDefaultMetadataPath();
    }

    /**
     * Returns the {@link DropBox} instance that determines how new files are
     * found.
     * 
     * @return the {@link DropBox} instance that determines how new files are
     *         found.
     */
    @XmlElement(name = "drop_box")
    public DropBox getDropBox() {
        return dropBox;
    }

    @Override
    @XmlElement(name = "duplication",
                defaultValue = ".default.")
    public Collection<String> getDuplications() {
        return super.getDuplications();
    }

    @Override
    @XmlElement(name = "local_id")
    public String getLocalId() {
        return super.getLocalId();
    }

    @Override
    @XmlTransient
    public String getNeighbor() {
        return null;
    }

    @Override
    @XmlElement(name = "outbound_transfer")
    public Collection<String> getOutboundTransfers() {
        return super.getOutboundTransfers();
    }

    /**
     * Returns true if SPADE owns the data files. That is to say, it must copy
     * them into the cache and then delete the originals.
     * 
     * @return true if SPADE owns the data files. That is to say, it must copy
     *         them into the cache and then delete the originals.
     */
    @XmlElement
    public Boolean getOwner() {
        return localOptions.getOwner();
    }

    @Override
    @XmlElement(defaultValue = "true")
    public Boolean getPack() {
        return super.getPack();
    }

    @Override
    @XmlElement(defaultValue = "true")
    public Boolean getEmbedded() {
        return super.getEmbedded();
    }

    @Override
    @XmlElement(defaultValue = "NORMAL")
    public HandlingPriority getPriority() {
        return super.getPriority();
    }

    @Override
    @XmlTransient
    public String getRemoteId() {
        return null;
    }

    @Override
    @XmlElement(name = "scan_limit")
    public Integer getScanLimit() {
        return super.getScanLimit();
    }

    @Override
    public Boolean getSubdirs() {
        return dropBox.getSubdirs();
    }

    @Override
    @XmlElement(defaultValue = "true")
    public Boolean getWarehouse() {
        return super.getWarehouse();
    }

    @Override
    protected void setAnalyze(final Boolean analyze) {
        super.setAnalyze(analyze);
    }

    @Override
    protected void setArchive(final FileCategory archive) {
        super.setArchive(archive);
    }

    @Override
    protected void setCompress(final Boolean compress) {
        super.setCompress(compress);
    }

    /**
     * Sets the path, if any, of the file containing the default metadata to use
     * for files matching this registration.
     * 
     * @param path
     *            the path, if any, of the file containing the default metadata
     *            to use for files matching this registration.
     */
    protected void setDefaultMetadataPath(final String path) {
        localOptions.setDefaultMetadataPath(path);
    }

    /**
     * Sets the {@link DropBox} instance that determines how new files are
     * found.
     * 
     * @param dropBox
     *            the {@link DropBox} instance that determines how new files are
     *            found.
     */
    protected void setDropBox(final DropBox dropBox) {
        this.dropBox = dropBox;
        setDropLocation(dropBox.getLocation());
        setPattern((dropBox.getPattern()).getPattern());
    }

    @Override
    protected void setDuplications(final Collection<String> duplications) {
        super.setDuplications(duplications);
    }

    @Override
    protected void setLocalId(final String localId) {
        super.setLocalId(localId);
    }

    @Override
    protected void setOutboundTransfers(final Collection<String> outboundTransfers) {
        super.setOutboundTransfers(outboundTransfers);
    }

    /**
     * Sets whether SPADE owns the data files or not
     * 
     * @param owner
     *            true if SPADE owns the data files.
     */
    protected void setOwner(final Boolean owner) {
        localOptions.setOwner(owner);
    }

    @Override
    protected void setPack(final Boolean pack) {
        super.setPack(pack);
    }

    @Override
    protected void setEmbedded(final Boolean packed) {
        super.setEmbedded(packed);
    }

    @Override
    protected void setPriority(final HandlingPriority priority) {
        super.setPriority(priority);
    }

    @Override
    protected void setScanLimit(final Integer limit) {
        super.setScanLimit(limit);
    }

    @Override
    protected void setWarehouse(final Boolean warehouse) {
        super.setWarehouse(warehouse);
    }

    @XmlTransient
    private void setWarehouseRoot(File warehouseRoot) {
        this.warehouseRoot = warehouseRoot;
    }

    // static member methods (alphabetic)

    /**
     * Returns a {@link LocalRegistration} instance read from an XML
     * representation of it provided by the specified {@link InputStream}.
     * 
     * @param stream
     *            the {@link InputStream} containing the XML representation.
     * 
     * @return the instance of this class read from the {@link InputStream}.
     */
    public static LocalRegistration read(final InputStream stream) {
        try {
            JAXBContext content = JAXBContext.newInstance(LocalRegistration.class);
            final Unmarshaller unmarshaller = content.createUnmarshaller();
            return (LocalRegistration) unmarshaller.unmarshal(stream);
        } catch (JAXBException e1) {
            e1.printStackTrace();
            return null;
        }
    }

    /**
     * Recursively instantiate all {@link LocalRegistration} instances in the
     * specified directory and below.
     * 
     * @param directory
     *            the directory from which to read the {@link LocalRegistration}
     *            instances.
     * @param warehouseRoot
     *            the {@link File} instance of the local warehouse so bundles
     *            can be transferred out of the warehouse.
     * 
     * @return the collection of {@link LocalRegistration} instances read.
     */
    public static List<LocalRegistration> readLocalFromTree(final File directory,
                                                            final File warehouseRoot) {
        List<LocalRegistration> result = new ArrayList<LocalRegistration>();
        final File[] files = directory.listFiles();
        if (null != files) {
            for (File file : files) {
                final String name = file.getName();
                if (file.isDirectory()
                    && !(name.startsWith(DISABLED_REGISTRATIONS_FLAG)
                         || name.endsWith(DISABLED_REGISTRATIONS_FLAG))) {
                    result.addAll(readLocalFromTree(file,
                                                    warehouseRoot));
                } else {
                    // Only consider XML files.
                    if (name.endsWith(".xml")
                        && !(name.startsWith(DISABLED_REGISTRATIONS_FLAG))) {
                        try {
                            final LocalRegistration registration = read(new FileInputStream(file));
                            if (null != registration) {
                                registration.setWarehouseRoot(warehouseRoot);
                                result.add(registration);
                            }
                        } catch (FileNotFoundException e) {
                            // Should not be able to get here!
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return result;
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
