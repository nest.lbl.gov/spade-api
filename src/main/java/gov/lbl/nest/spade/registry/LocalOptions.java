package gov.lbl.nest.spade.registry;

import java.io.File;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlTransient;

/**
 * This class is used to contain the setting of available ingestion options for
 * a local Bundle.
 * 
 * @author patton
 */
public class LocalOptions extends
                          Options {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link File}, if any, of the file containing the default metadata to
     * use for files matching this registration.
     */
    private File defaultMetadata;

    /**
     * True if SPADE owns the data files. That is to say, it must copy them into
     * the cache and then delete the originals.
     */
    private Boolean owner;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected LocalOptions() {
        // Required for JAXB
    }

    /**
     * Creates an instance of this class
     * 
     * @param rhs
     *            the {@link LocalOptions} instance from which to make this
     *            object.
     */
    private LocalOptions(final LocalOptions rhs) {
        super(rhs);
        defaultMetadata = rhs.defaultMetadata;
        owner = rhs.owner;
    }

    // instance member method (alphabetic)

    @Override
    public boolean cacheData() {
        if (null == owner || Boolean.TRUE == owner) {
            return true;
        }
        return false;
    }

    @Override
    public boolean createMetadata() {
        final Boolean embedded = getEmbedded();
        if (null == embedded || Boolean.FALSE == embedded) {
            return true;
        }
        return false;
    }

    @Override
    public Options copy() {
        return new LocalOptions(this);
    }

    /**
     * Returns the compressed data file, if any, for this ticket.
     * 
     * @return the compressed data file, if any, for this ticket.
     */
    @XmlTransient
    public File getDefaultMetadataFile() {
        return defaultMetadata;
    }

    /**
     * Returns the path the compressed file for this ticket.
     * 
     * @return the path the compressed file for this ticket.
     */
    @XmlElement(name = "default_metadata")
    protected String getDefaultMetadataPath() {
        if (null == defaultMetadata) {
            return null;
        }
        return defaultMetadata.getAbsolutePath();
    }

    /**
     * Returns true if SPADE owns the data files. That is to say, it must copy
     * them into the cache and then delete the originals.
     * 
     * @return true if SPADE owns the data files. That is to say, it must copy
     *         them into the cache and then delete the originals.
     */
    @XmlElement
    public Boolean getOwner() {
        return owner;
    }

    /**
     * Sets the path, if any, of the file containing the default metadata to use
     * for files matching this registration.
     * 
     * @param path
     *            the path, if any, of the file containing the default metadata
     *            to use for files matching this registration.
     */
    protected void setDefaultMetadataPath(final String path) {
        if (null == path) {
            defaultMetadata = null;
            return;
        }
        defaultMetadata = new File(path);
    }

    /**
     * Sets whether SPADE owns the data files or not
     * 
     * @param owner
     *            true if SPADE owns the data files.
     */
    protected void setOwner(Boolean owner) {
        this.owner = owner;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
