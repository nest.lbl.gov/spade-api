package gov.lbl.nest.spade.registry;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * This class implements the {@link DataLocator} interface to generate the data
 * file by substituting the semaphore suffix with the data one.
 * 
 * @author patton
 */
public class SuffixSubstitution implements
                                DataLocator {

    /**
     * This class captures the data-suffix mapping.
     */
    public static class Mapping {

        /**
         * The data suffix.
         */
        protected final String data;

        /**
         * The semaphore suffix.
         */
        protected final String semaphore;

        /**
         * This creates a new instance of this class.
         * 
         * @param data
         *            the data suffix.
         * @param semaphore
         *            the semaphore suffix.
         */
        public Mapping(final String data,
                       final String semaphore) {
            this.data = data;
            this.semaphore = semaphore;
        }
    }
    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The data <-> semaphore suffix mappings.
     */
    private final List<Mapping> mappings = new ArrayList<Mapping>();

    // constructors

    /**
     * Creates an instance of this class.
     * 
     * @param semaphoreSuffix
     *            the suffix used on semaphore files.
     * @param dataSuffix
     *            the suffix used on data files.
     */
    protected SuffixSubstitution(final String semaphoreSuffix,
                                 final String dataSuffix) {
        mappings.add(new Mapping(dataSuffix,
                                 semaphoreSuffix));
    }

    // instance member method (alphabetic)

    @Override
    public String getBundleName(final String semaphoreName) {
        for (Mapping mapping : mappings) {
            final String semaphoreSuffix = mapping.semaphore;
            if (semaphoreName.endsWith(semaphoreSuffix)) {
                return semaphoreName.substring(0,
                                               semaphoreName.length() - semaphoreSuffix.length());
            }
        }
        throw new IllegalArgumentException("The semaphore file \"" + semaphoreName
                                           + "\" can not be handled by the \""
                                           + (this.getClass()).getSimpleName()
                                           + "\" class.");
    }

    /**
     * Returns the data file name created by the suffix substitution.
     *
     * @param semaphoreName
     *            the semaphoreName of the semaphore file whose data file should be
     *            returned.
     * 
     * @return the data file name created by the suffix substitution.
     */
    protected String getDataFileFromSemaphoreName(final String semaphoreName) {
        for (Mapping mapping : mappings) {
            if (semaphoreName.endsWith(mapping.semaphore)) {
                return semaphoreName.substring(0,
                                               semaphoreName.length() - (mapping.semaphore).length())
                       + mapping.data;
            }
        }
        throw new IllegalArgumentException("Semaphore File can not be handled by the \"" + (this.getClass()).getSimpleName()
                                           + "\" class.");
    }

    /**
     * Returns the suffix used on data files.
     *
     * @param dataFile
     *            the name of the data file whose data suffix should be returned.
     * 
     * @return the suffix used on data files.
     */
    protected String getDataSuffix(final String dataFile) {
        for (Mapping mapping : mappings) {
            if (dataFile.endsWith(mapping.data)) {
                return mapping.data;
            }
        }
        return null;
    }

    /**
     * Returns the suffix used on semaphore files.
     * 
     * @param dataFile
     *            the name of the data file whose semaphore suffix should be
     *            returned.
     *
     * @return the suffix used on semaphore files.
     */
    protected String getSemaphoreSuffix(final String dataFile) {
        for (Mapping mapping : mappings) {
            if (dataFile.endsWith(mapping.data)) {
                return mapping.semaphore;
            }
        }
        return null;
    }

    @Override
    public List<ExternalFile> locateData(final ExternalFile externalSemaphore,
                                         final File internalSemaphore,
                                         final String identity) {
        final List<ExternalFile> result = new ArrayList<ExternalFile>();
        final String dataFile = getDataFileFromSemaphoreName(externalSemaphore.getName());
        result.add(new ExternalFile(externalSemaphore.getLocation(),
                                    dataFile,
                                    new File(dataFile)));
        return result;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
