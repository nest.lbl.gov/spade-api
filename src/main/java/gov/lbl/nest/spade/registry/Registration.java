package gov.lbl.nest.spade.registry;

import java.util.Collection;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import jakarta.xml.bind.annotation.XmlTransient;

/**
 * This class is used to define how a bundle should be managed and also how find
 * new ones.
 * 
 * @author patton
 */
public abstract class Registration {

    // public static final member data

    // protected static final member data

    /**
     * Registrations below this directory are not loaded.
     */
    protected static final String DISABLED_REGISTRATIONS_FLAG = "disabled";

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The directory assigned to be the DropBox area for this registration.
     */
    private ExternalLocation dropLocation;

    /**
     * The unique identity of this object within the set of local
     * {@link Registration} instances.
     */
    private String localId;

    /**
     * The values of the ingest options for this registration.
     */
    private Options options;

    /**
     * The {@link Pattern} instance that must be matched by a file name in order for
     * it to match this registration.
     */
    private Pattern pattern;

    /**
     * The {@link Predicate} instance used to find file names that match the
     * {@link #pattern}.
     */
    private Predicate<String> predicate;

    // constructors

    /**
     * Creates an instance of this class.
     * 
     * @param options
     *            the {@link Options} instance used by this class.
     */
    protected Registration(final Options options) {
        this.options = options;
    }

    /**
     * Creates an instance of this class to test purposes.
     * 
     * @param localId
     *            the unique identity of this object within the set of local
     *            {@link Registration} instances.
     */
    public Registration(final String localId) {
        setLocalId(localId);
    }

    // instance member method (alphabetic)

    /**
     * Returns true if files matching this registration should be analyzed locally.
     * 
     * @return true if files matching this registration should be analyzed locally.
     */
    public Boolean getAnalyze() {
        return options.getAnalyze();
    }

    /**
     * Returns true if files matching this registration should be archived locally.
     * 
     * @return true if files matching this registration should be archived locally.
     */
    public FileCategory getArchive() {
        return options.getArchive();
    }

    /**
     * Returns true if files matching this registration should be compressed.
     * 
     * @return true if files matching this registration should be compressed.
     */
    public Boolean getCompress() {
        return options.getCompress();
    }

    /**
     * Return the {@link DataLocator} instance to be used to map a semaphore file
     * onto its data files.
     * 
     * @return the {@link DataLocator} instance to be used to map a semaphore file
     *         onto its data files.
     */
    public abstract DataLocator getDataLocator();

    /**
     * Returns the directory assigned to be the DropBox area for this registration.
     * 
     * @return the directory assigned to be the DropBox area for this registration.
     */
    public ExternalLocation getDropLocation() {
        return dropLocation;
    }

    /**
     * Returns the collection of duplications by which files matching this
     * registration should be duplicated out of this application's management.
     * 
     * @return the collection of duplications by which files matching this
     *         registration should be transferred outbound from this application.
     */
    public Collection<String> getDuplications() {
        return options.getDuplications();
    }

    /**
     * Returns the unique identity of this object within the set of local
     * {@link Registration} instances.
     * 
     * @return the unique identity of this object within the set of local
     *         {@link Registration} instances.
     */
    public String getLocalId() {
        return localId;
    }

    /**
     * Returns the name of the neighboring SPADE deployment if this is not a local
     * registration, <code>null</code> otherwise.
     * 
     * @return the name of the neighboring SPADE deployment if this is not a local
     *         registration, <code>null</code> otherwise.
     */
    public abstract String getNeighbor();

    /**
     * Returns the values of the ingest options for this registration.
     * 
     * @return the values of the ingest options for this registration.
     */
    public Options getOptions() {
        return options;
    }

    /**
     * Returns the collection of outbound transfers by which files matching this
     * registration should be transferred outbound from this application.
     * 
     * @return the collection of outbound transfers by which files matching this
     *         registration should be transferred outbound from this application.
     */
    public Collection<String> getOutboundTransfers() {
        return options.getOutboundTransfers();
    }

    /**
     * Returns true if files matching this registration should be packed. Note,
     * files being compressed are automatically packed, and files already packed
     * will not be packed again.
     * 
     * @return true if files matching this registration should be packed. Note,
     *         files being compressed are automatically packed.
     */
    public Boolean getPack() {
        return options.getPack();
    }

    /**
     * Returns the {@link Pattern} instance that must be matched by a file name in
     * order for it to match this registration.
     * 
     * @return the {@link Pattern} instance that must be matched by a file name in
     *         order for it to match this registration.
     */
    public Pattern getPattern() {
        return pattern;
    }

    /**
     * Returns the {@link Predicate} instance used to find file names that match the
     * {@link #pattern}.
     * 
     * @return the {@link Predicate} instance used to find file names that match the
     *         {@link #pattern}.
     */
    @XmlTransient
    public Predicate<String> getPredicate() {
        return predicate;
    }

    /**
     * Returns the priority with which files matching this registration should be
     * handled.
     * 
     * @return the priority with which files matching this registration should be
     *         handled.
     */
    public HandlingPriority getPriority() {
        return options.getPriority();
    }

    /**
     * Returns true if files matching this registration have their metadata embedded
     * in themselves.
     *
     * @return true if files matching this registration have their metadata embedded
     *         in themselves.
     */
    public Boolean getEmbedded() {
        return options.getEmbedded();
    }

    /**
     * Returns the unique identity of this object within the set of
     * {@link Registration} on the delivering SPADE instance, nor <code>null</code>
     * is this has not been delivered.
     * 
     * @return the unique identity of this object within the set of
     *         {@link Registration} on the delivering SPADE instance.
     */
    public abstract String getRemoteId();

    /**
     * Returns the maximum number of bundles that can be fetched during a single
     * scan.
     * 
     * @return the maximum number of bundles that can be fetched during a single
     *         scan.
     */
    public Integer getScanLimit() {
        return options.getScanLimit();
    }

    /**
     * Returns <code>false</code> if sub-directories of this object should
     * <em>not</em> be searched.
     * 
     * @return <code>false</code> if sub-directories of this object should
     *         <em>not</em> be searched.
     */
    public abstract Boolean getSubdirs();

    /**
     * Returns true if files matching this registration should be warehoused
     * locally.
     * 
     * @return true if files matching this registration should be warehoused
     *         locally.
     */
    public Boolean getWarehouse() {
        return options.getWarehouse();
    }

    /**
     * Sets whether files matching this registration should be analyzed locally or
     * not.
     * 
     * @param analyze
     *            true if files matching this registration should be analyzed
     *            locally.
     */
    protected void setAnalyze(final Boolean analyze) {
        options.setAnalyze(analyze);
    }

    /**
     * Sets whether files matching this registration should be archived locally or
     * not.
     * 
     * @param archive
     *            true if files matching this registration should be archived
     *            locally.
     */
    protected void setArchive(final FileCategory archive) {
        options.setArchive(archive);
    }

    /**
     * Sets whether files matching this registration should be compressed or not.
     * 
     * @param compress
     *            true if files matching this registration should be compressed.
     */
    protected void setCompress(final Boolean compress) {
        options.setCompress(compress);
    }

    /**
     * Sets the directory assigned to be the DropBox area for this registration.
     * 
     * @param location
     *            the directory assigned to be the DropBox area for this
     *            registration.
     */
    protected void setDropLocation(final ExternalLocation location) {
        this.dropLocation = location;
    }

    /**
     * Sets the collection of duplications by which files matching this registration
     * should be duplicated out of this application's management.
     * 
     * @param duplications
     *            the collection of duplications by which files matching this
     *            registration should be duplicated out of this application's
     *            management.
     */
    protected void setDuplications(final Collection<String> duplications) {
        options.setDuplications(duplications);
    }

    /**
     * Sets whether files matching this registration have their metadata embedded in
     * themselves or not.
     *
     * @param embedded
     *            true if files matching this registration have their metadata
     *            embedded in themselves.
     */
    protected void setEmbedded(final Boolean embedded) {
        options.setEmbedded(embedded);
    }

    /**
     * Sets the unique identity of this object within the set of local
     * {@link Registration} instances.
     * 
     * @param localId
     *            the unique identity of this object within the set of local
     *            {@link Registration} instances.
     */
    protected void setLocalId(final String localId) {
        this.localId = localId;
    }

    /**
     * Sets the collection of outbound transfers by which files matching this
     * registration should be transferred outbound from this application.
     * 
     * @param outboundTransfers
     *            the collection of outbound transfers by which files matching this
     *            registration should be transferred outbound from this application.
     */
    protected void setOutboundTransfers(final Collection<String> outboundTransfers) {
        options.setOutboundTransfers(outboundTransfers);
    }

    /**
     * Sets whether files matching this registration should be packed or not.
     * 
     * @param pack
     *            true if files matching this registration should be packed.
     */
    protected void setPack(final Boolean pack) {
        options.setPack(pack);
    }

    /**
     * Sets the {@link Pattern} instance that must be matched by a file name in
     * order for it to match this registration.
     * 
     * @param pattern
     *            the {@link Pattern} instance that must be matched by a file name
     *            in order for it to match this registration.
     */
    protected void setPattern(final Pattern pattern) {
        if (null == pattern) {
            predicate = null;
        } else {
            predicate = new Predicate<String>() {

                @Override
                public boolean test(String t) {
                    return (pattern.matcher(t)).matches();
                }
            };
        }
        this.pattern = pattern;
    }

    /**
     * Sets the priority with which files matching this registration should be
     * handled.
     * 
     * @param priority
     *            the priority with which files matching this registration should be
     *            handled.
     */
    protected void setPriority(HandlingPriority priority) {
        options.setPriority(priority);
    }

    /**
     * Sets the maximum number of bundles that can be fetched during a single scan.
     * 
     * @param limit
     *            the maximum number of bundles that can be fetched during a single
     *            scan.
     */
    protected void setScanLimit(Integer limit) {
        options.setScanLimit(limit);
    }

    /**
     * Sets whether files matching this registration should be warehoused locally or
     * not
     * 
     * @param warehouse
     *            true if files matching this registration should be warehoused
     *            locally.
     */
    protected void setWarehouse(final Boolean warehouse) {
        options.setWarehouse(warehouse);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
