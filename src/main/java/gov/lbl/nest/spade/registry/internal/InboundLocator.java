package gov.lbl.nest.spade.registry.internal;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import gov.lbl.nest.spade.registry.DataLocator;
import gov.lbl.nest.spade.registry.ExternalFile;
import gov.lbl.nest.spade.registry.FileCategory;

/**
 * This class implements the {@link DataLocator} interface in order to find
 * inbound bundles.
 * 
 * @author patton
 */
public class InboundLocator implements
                            DataLocator {

    // public static final member data

    /**
     * The separator used in the inbound filenames.
     */
    public static final String SEPARATOR = "_";

    /**
     * The prefix attached to transferred files.
     */
    public static final String V4_PREFIX = "v4";

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The regex index of the format version of the inbound semaphore file.
     */
    private static final int VERSION_INDEX = 1;

    /**
     * The regex index of the remote ticket value.
     */
    private static final int TICKET_INDEX = VERSION_INDEX + 1;

    /**
     * The regex index of the remote registration value.
     */
    public static final int FILECATEGORY_INDEX = TICKET_INDEX + 1;

    /**
     * The regex index of the date file name.
     */
    public static final int PAYLOAD_INDEX = FILECATEGORY_INDEX + 1;

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    @Override
    public String getBundleName(final String semaphoreName) {
        final InboundSemaphore inboundSemaphore = new InboundSemaphore(semaphoreName);
        return inboundSemaphore.bundle;
    }

    @Override
    public List<ExternalFile> locateData(final ExternalFile externalSemaphore,
                                         final File internalSemaphore,
                                         final String identity) {
        final InboundSemaphore inboundSemaphore = new InboundSemaphore(externalSemaphore.getName());
        final List<ExternalFile> result = new ArrayList<ExternalFile>();

        final String ticket = inboundSemaphore.ticket;
        final String externalTransfer = (payloadPattern(inboundSemaphore.version,
                                                        ticket)).pattern();
        final File internalTransfer = new File(inboundSemaphore.bundle);
        final ExternalFile transferFile = new ExternalFile(externalSemaphore.getLocation(),
                                                           externalTransfer,
                                                           internalTransfer,
                                                           "$" + PAYLOAD_INDEX);
        result.add(transferFile);

        final String metadataFilename = InternalFileName.getMetadataFilename(inboundSemaphore.bundle);
        final String externalMetadata = inboundSemaphore.version + InboundLocator.SEPARATOR
                                        + ticket
                                        + InboundLocator.SEPARATOR
                                        + FileCategory.METADATA
                                        + InboundLocator.SEPARATOR
                                        + metadataFilename;
        final File internalMetadata = new File("..",
                                               metadataFilename);
        final ExternalFile metadataFile = new ExternalFile(externalSemaphore.getLocation(),
                                                           externalMetadata,
                                                           internalMetadata);
        result.add(metadataFile);

        return result;
    }

    // static member methods (alphabetic)

    /**
     * Returns the String used to at the {@link Pattern} for inbound payload
     * files.
     * 
     * @param version
     *            the version of file name formats.
     * @param ticket
     *            the identity of the ticket whose pattern should be returned.
     * @return the String used to at the {@link Pattern} for inbound payload
     *         files for the specified ticket.
     */
    public static Pattern payloadPattern(final String version,
                                         final String ticket) {
        return Pattern.compile("(" + version
                               + ")"
                               + SEPARATOR
                               + "("
                               + ticket
                               + ")"
                               + SEPARATOR
                               + "("
                               + FileCategory.DATA
                               + "|"
                               + FileCategory.EMBEDDED
                               + "|"
                               + FileCategory.PACKED
                               + "|"
                               + FileCategory.COMPRESSED
                               + ")"
                               + SEPARATOR
                               + "(.*)$");
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
