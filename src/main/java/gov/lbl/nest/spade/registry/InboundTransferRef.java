package gov.lbl.nest.spade.registry;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class describes how a bundle will be delivered from a specified origin.
 * 
 * @author patton
 */
@XmlType(propOrder = { "name",
                       "remoteId" })
public class InboundTransferRef {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The name of the inbound transfer this object references.
     */
    private String name;

    /**
     * The localId on the remote SPADE whose files are handled by this
     * registration.
     */
    private String remoteId;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected InboundTransferRef() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param name
     *            the name of the inbound transfer this object references.
     */
    public InboundTransferRef(final String name) {
        setName(name);
    }

    // instance member method (alphabetic)

    /**
     * Returns the name of the inbound transfer this object references.
     * 
     * @return the name of the inbound transfer this object references.
     */
    @XmlElement
    public String getName() {
        return name;
    }

    /**
     * Returns the localId on the remote SPADE whose files are handled by this
     * registration.
     * 
     * @return the localId on the remote SPADE whose files are handled by this
     *         registration.
     */
    @XmlElement(name = "remote_id")
    public String getRemoteId() {
        return remoteId;
    }

    /**
     * Sets the name of the inbound transfer this object references.
     * 
     * @param name
     *            the name of the inbound transfer this object references.
     */
    protected void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the localId on the remote SPADE whose files are handled by this
     * registration.
     * 
     * @param remoteId
     *            the localId on the remote SPADE whose files are handled by
     *            this registration.
     */
    protected void setRemoteId(String remoteId) {
        this.remoteId = remoteId;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
