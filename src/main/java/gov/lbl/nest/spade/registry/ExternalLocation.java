package gov.lbl.nest.spade.registry;

import java.io.File;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class provides details of the directory assigned to be the DropBox area
 * for a registration.
 * 
 * @author patton
 */
@XmlType(propOrder = { "host",
                       "directory" })
public class ExternalLocation {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The String used to separate a host for a path on that host.
     */
    private static final String HOST_SEPARATOR = ":";

    /**
     * The name of the system property that contains the users home directory.
     */
    private static final String USER_HOME_PROPERTY = "user.home";

    /**
     * The string used to signify the users home directory.
     */
    private static final String USER_HOME_STRING = "~";

    // private static member data

    // private instance member data

    /**
     * The value of the directory property.
     */
    private String directory;

    /**
     * The value of the host property.
     */
    private String host;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected ExternalLocation() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param path
     *            the path, including any host, to the DropBox area.
     */
    public ExternalLocation(String path) {
        final String[] parts = path.split(":",
                                          2);
        if (1 == parts.length) {
            setHost(null);
            setDirectory(parts[0]);
        } else {
            setHost(parts[0]);
            setDirectory(parts[1]);
        }
    }

    /**
     * Create an instance of this file.
     * 
     * @param rhs
     *            the {@link ExternalLocation} to copy.
     */
    public ExternalLocation(final ExternalLocation rhs) {
        directory = rhs.getDirectory();
        host = rhs.getHost();
    }

    /**
     * Creates an instance of this file.
     * 
     * @param host
     *            the value of the host property.
     * @param directory
     *            the value of the directory property.
     */
    public ExternalLocation(final String host,
                            final String directory) {
        this.directory = directory;
        this.host = host;
    }

    // instance member method (alphabetic)

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof ExternalLocation))
            return false;
        ExternalLocation other = (ExternalLocation) obj;
        if (getResolvedDirectory() == null) {
            if (other.getResolvedDirectory() != null)
                return false;
        } else if (!getResolvedDirectory().equals(other.getResolvedDirectory()))
            return false;
        if (getHost() == null) {
            if (other.getHost() != null)
                return false;
        } else if (!getHost().equals(other.getHost()))
            return false;
        return true;
    }

    /**
     * Returns the value of the directory property.
     * 
     * @return the value of the directory property.
     */
    @XmlElement
    public String getDirectory() {
        return directory;
    }

    /**
     * Returns the value of the host property.
     * 
     * @return the value of the host property.
     */
    @XmlElement
    public String getHost() {
        return host;
    }

    /**
     * Returns the value of the directory property resolved for any leading "~".
     * 
     * @return the value of the directory property resolved for any leading "~".
     */
    @XmlTransient
    public String getResolvedDirectory() {
        final String path = directory;
        return resolvePath(path);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getResolvedDirectory() == null) ? 0
                                                                    : getResolvedDirectory().hashCode());
        result = prime * result + ((getHost() == null) ? 0
                                                       : getHost().hashCode());
        return result;
    }

    /**
     * Sets the value of the directory property.
     * 
     * @param directory
     *            the value of the directory property.
     */
    protected void setDirectory(String directory) {
        this.directory = directory;
    }

    /**
     * Sets the value of the host property.
     * 
     * @param host
     *            the value of the host property.
     */
    protected void setHost(String host) {
        this.host = host;
    }

    // static member methods (alphabetic)

    /**
     * Returns the path to the users home directory.
     * 
     * @return the path to the users home directory.
     */
    public static String getUserHome() {
        return System.getProperty(USER_HOME_PROPERTY);
    }

    /**
     * Returns the value of the specified path resolved for any leading "~".
     * 
     * @param path
     *            the path to be resolved.
     * @return the value of the specified path resolved for any leading "~".
     */
    public static String resolvePath(final String path) {
        if (null != path && path.startsWith(USER_HOME_STRING + File.separator)) {
            final String userHome = System.getProperty(USER_HOME_PROPERTY);
            return userHome + path.substring(USER_HOME_STRING.length());
        }
        return path;
    }

    // Description of this object.
    @Override
    public String toString() {
        if (null == host) {
            return directory;
        }
        return host + HOST_SEPARATOR
               + directory;
    }

    // public static void main(String args[]) {}
}
