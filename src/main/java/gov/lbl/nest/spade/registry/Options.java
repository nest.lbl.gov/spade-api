package gov.lbl.nest.spade.registry;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to contain the setting of available ingestion options for
 * a Bundle.
 * 
 * This can not be abstract as JAXP can not (un)marshal abstract classes.
 * 
 * @author patton
 */
@XmlType(propOrder = { "priority",
                       "scanLimit",
                       "analyze",
                       "archive",
                       "duplications",
                       "compress",
                       "embedded",
                       "expand",
                       "outboundTransfers",
                       "pack",
                       "single",
                       "unpack",
                       "warehouse" })
@XmlSeeAlso({ ArchiveOptions.class,
              InboundOptions.class,
              LocalOptions.class,
              RetryOptions.class })
public class Options {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * True if files matching this registration should be analyzed locally.
     */
    private Boolean analyze;

    /**
     * The category of files matching this registration should be archived locally.
     */
    private FileCategory archive;

    /**
     * True if files matching this registration should be compressed.
     */
    private Boolean compress;

    /**
     * The collection of duplications by which files matching this registration
     * should be transferred out of this application.
     */
    private Collection<String> duplications;

    /**
     * True if files matching this registration have their metadata embedded in
     * themselves.
     */
    private Boolean embedded;

    /**
     * True if files matching this registration should be expanded.
     */
    private Boolean expand;

    /**
     * The collection of outbound transfers by which files matching this
     * registration should be transferred outbound from this application.
     */
    private Collection<String> outboundTransfers;

    /**
     * True if files matching this registration should be packed.
     */
    private Boolean pack;

    /**
     * The priority with which files matching this registration should be handled.
     */
    private HandlingPriority priority;

    /**
     * The maximum number of bundles that can be fetched during a single scan.
     */
    private Integer scanLimit;

    /**
     * True if the data payload matching this registration is always only a single
     * file.
     */
    private Boolean single;

    /**
     * True if files matching this registration should be unpacked.
     */
    private Boolean unpack;

    /**
     * True if files matching this registration should be warehoused locally.
     */
    private Boolean warehouse;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Options() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param single
     *            true if the data payload matching this registration is a single
     *            file.
     */
    public Options(final Boolean single) {
        setSingle(single);
    }

    /**
     * Creates an instance of this class.
     * 
     * @param rhs
     *            the {@link Options} instance from which to make this object.
     */
    protected Options(final Options rhs) {
        analyze = rhs.analyze;
        archive = rhs.archive;
        compress = rhs.compress;
        duplications = rhs.duplications;
        embedded = rhs.embedded;
        expand = rhs.expand;
        outboundTransfers = rhs.outboundTransfers;
        pack = rhs.pack;
        priority = rhs.priority;
        scanLimit = rhs.scanLimit;
        single = rhs.single;
        unpack = rhs.unpack;
        warehouse = rhs.warehouse;
    }

    // instance member method (alphabetic)

    /**
     * Returns true if this SPADE instance should "move" the data files into is
     * cache. This will delete the external copy of the file.
     * 
     * @return true if this SPADE instance should "move" the data files into is
     *         cache.
     */
    public boolean cacheData() {
        throw new UnsupportedOperationException("A concrete subclass is required to implement this operation");
    }

    /**
     * Returns true if this SPADE instance should create the metadata for this
     * bundle.
     * 
     * @return true if this SPADE instance should create the metadata for this
     *         bundle.
     */
    public boolean createMetadata() {
        throw new UnsupportedOperationException("A concrete subclass is required to implement this operation");
    }

    /**
     * Returns a copy of this object.
     * 
     * @return a copy of this object.
     */
    public Options copy() {
        return new Options(this);
    }

    /**
     * Returns true if files matching this registration should be analyzed locally.
     * 
     * @return true if files matching this registration should be analyzed locally.
     */
    @XmlElement
    public Boolean getAnalyze() {
        return analyze;
    }

    /**
     * Returns the category of files matching this registration should be archived
     * locally.
     * 
     * @return the category of files matching this registration should be archived
     *         locally.
     */
    @XmlElement(defaultValue = "DATA")
    public FileCategory getArchive() {
        return archive;
    }

    /**
     * Returns true if files matching this registration should be compressed.
     * 
     * @return true if files matching this registration should be compressed.
     */
    @XmlElement
    public Boolean getCompress() {
        return compress;
    }

    /**
     * Returns the collection of duplications by which files matching this
     * registration should be duplicated out of this application's management.
     * 
     * @return the collection of duplications by which files matching this
     *         registration should be transferred outbound from this application.
     */
    @XmlElement(name = "duplication")
    public Collection<String> getDuplications() {
        return duplications;
    }

    /**
     * Returns true if files matching this registration have their metadata embedded
     * in themselves.
     * 
     * @return true if files matching this registration have their metadata embedded
     *         in themselves.
     */
    @XmlElement
    public Boolean getEmbedded() {
        return embedded;
    }

    /**
     * Returns true if files matching this registration should be expanded.
     * 
     * @return true if files matching this registration should be expanded.
     */
    @XmlElement
    public Boolean getExpand() {
        return expand;
    }

    /**
     * Returns the collection of outbound transfers by which files matching this
     * registration should be transferred outbound from this application.
     * 
     * @return the collection of outbound transfers by which files matching this
     *         registration should be transferred outbound from this application.
     */
    @XmlElement(name = "outbound_transfers")
    public Collection<String> getOutboundTransfers() {
        return outboundTransfers;
    }

    /**
     * Returns true if files matching this registration should be packed. Note,
     * files being compressed are automatically packed, and files already packed
     * will not be packed again.
     * 
     * @return true if files matching this registration should be packed.
     */
    @XmlElement
    public Boolean getPack() {
        if (null != embedded && Boolean.TRUE == embedded) {
            return Boolean.FALSE;
        }
        if (null != compress && Boolean.TRUE == compress) {
            return Boolean.TRUE;
        }
        return pack;
    }

    /**
     * Returns the priority with which files matching this registration should be
     * handled.
     * 
     * @return the priority with which files matching this registration should be
     *         handled.
     */
    @XmlElement(defaultValue = "NORMAL")
    public HandlingPriority getPriority() {
        return priority;
    }

    /**
     * Returns true if files matching this registration should be unpacked.
     * 
     * @return true if files matching this registration should be unpacked.
     */
    @XmlElement
    public Boolean getUnpack() {
        return unpack;
    }

    /**
     * Returns the maximum number of bundles that can be fetched during a single
     * scan.
     * 
     * @return the maximum number of bundles that can be fetched during a single
     *         scan.
     */
    @XmlElement(name = "scan_limit")
    public Integer getScanLimit() {
        return scanLimit;
    }

    /**
     * Returns true if files matching this registration should be warehoused
     * locally. <code>null</code> implies true.
     * 
     * @return true if files matching this registration should be warehoused
     *         locally.
     */
    @XmlElement(defaultValue = "true")
    public Boolean getWarehouse() {
        if (null == warehouse) {
            return Boolean.TRUE;
        }
        return warehouse;
    }

    /**
     * Returns true if the payload matching this registration is always only a
     * single file.
     * 
     * @return true if the payload matching this registration is always only a
     *         single file.
     */
    @XmlElement(defaultValue = "true")
    public Boolean isSingle() {
        return single;
    }

    /**
     * Merges the supplied {@link Options} instance with this one, returning the
     * merged instance.
     * 
     * @param rhs
     *            the {@link Options} instance to be merged with this one.
     * 
     * @return the merged instance.
     */
    public Options merge(final Options rhs) {

        if (null == analyze || !analyze.booleanValue()) {
            analyze = rhs.analyze;
        }

        if (null == archive) {
            archive = rhs.archive;
        } else if (null != rhs.archive && archive != rhs.archive) {
            throw new IllegalArgumentException();
        }

        if (null == compress || !compress.booleanValue()) {
            compress = rhs.compress;
        }

        if (null == duplications) {
            duplications = rhs.duplications;
        } else if (null != rhs.duplications) {
            duplications = merge(duplications,
                                 rhs.duplications);
        }

        if (null == expand || !expand.booleanValue()) {
            expand = rhs.expand;
        }

        if (null == outboundTransfers) {
            outboundTransfers = rhs.outboundTransfers;
        } else if (null != rhs.outboundTransfers) {
            outboundTransfers = merge(outboundTransfers,
                                      rhs.outboundTransfers);
        }

        if (null == pack || !pack.booleanValue()) {
            pack = rhs.pack;
        }

        if (null == scanLimit || (null != rhs.scanLimit && rhs.scanLimit.intValue() > scanLimit.intValue())) {
            scanLimit = rhs.scanLimit;
        }

        if (null == single || !single.booleanValue()) {
            single = rhs.single;
        }

        if (null == unpack || !unpack.booleanValue()) {
            unpack = rhs.unpack;
        }

        if (null == warehouse || !warehouse.booleanValue()) {
            warehouse = rhs.warehouse;
        }
        return this;
    }

    /**
     * Sets whether files matching this registration should be analyzed locally or
     * not.
     * 
     * @param analyze
     *            true if files matching this registration should be analyzed
     *            locally.
     */
    protected void setAnalyze(final Boolean analyze) {
        this.analyze = analyze;
    }

    /**
     * Sets the category of files matching this registration should be archived
     * locally.
     * 
     * @param archive
     *            the {@link FileCategory} of files matching this registration
     *            should be archived locally.
     */
    protected void setArchive(final FileCategory archive) {
        this.archive = archive;
    }

    /**
     * Sets whether files matching this registration should be compressed or not.
     * 
     * @param compress
     *            true if files matching this registration should be compressed.
     */
    protected void setCompress(final Boolean compress) {
        this.compress = compress;
    }

    /**
     * Sets the collection of duplications by which files matching this registration
     * should be duplicated out of this application's management.
     * 
     * @param duplications
     *            the collection of duplications by which files matching this
     *            registration should be duplicated out of this application's
     *            management.
     */
    protected void setDuplications(final Collection<String> duplications) {
        this.duplications = duplications;
    }

    /**
     * Sets whether files matching this registration have their metadata embedded in
     * themselves.not.
     * 
     * @param embedded
     *            true if files matching this registration have their metadata
     *            embedded in themselves.
     */
    public void setEmbedded(final Boolean embedded) {
        this.embedded = embedded;
    }

    /**
     * Sets whether files matching this registration should be expanded or not.
     * 
     * @param expand
     *            true if files matching this registration should be expanded.
     */
    public void setExpand(final Boolean expand) {
        this.expand = expand;
    }

    /**
     * Sets the collection of outbound transfers by which files matching this
     * registration should be transferred outbound from this application.
     * 
     * @param outboundTransfers
     *            the collection of outbound transfers by which files matching this
     *            registration should be transferred outbound from this application.
     */
    public void setOutboundTransfers(final Collection<String> outboundTransfers) {
        this.outboundTransfers = outboundTransfers;
    }

    /**
     * Sets whether files matching this registration should be packed or not.
     * 
     * @param pack
     *            true if files matching this registration should be packed.
     */
    protected void setPack(final Boolean pack) {
        this.pack = pack;
    }

    /**
     * Sets the priority with which files matching this registration should be
     * handled.
     * 
     * @param priority
     *            the priority with which files matching this registration should be
     *            handled.
     */
    protected void setPriority(HandlingPriority priority) {
        this.priority = priority;
    }

    /**
     * Sets the maximum number of bundles that can be fetched during a single scan.
     * 
     * @param limit
     *            the maximum number of bundles that can be fetched during a single
     *            scan.
     */
    protected void setScanLimit(Integer limit) {
        scanLimit = limit;
    }

    /**
     * Sets whether the payload matching this registration is always only a single
     * file or not.
     * 
     * @param single
     *            true if the payload matching this registration is always only a
     *            single file.
     */
    public void setSingle(Boolean single) {
        this.single = single;
    }

    /**
     * Sets whether files matching this registration should be unpacked or not.
     * 
     * @param unpack
     *            true if files matching this registration should be unpacked.
     */
    public void setUnpack(final Boolean unpack) {
        this.unpack = unpack;
    }

    /**
     * Sets whether files matching this registration should be warehoused locally or
     * not
     * 
     * @param warehouse
     *            true if files matching this registration should be warehoused
     *            locally.
     */
    protected void setWarehouse(final Boolean warehouse) {
        this.warehouse = warehouse;
    }

    // static member methods (alphabetic)

    /**
     * Merges the supplied {@link String} collections, returning a new, merge,
     * {@link Collection} instance.
     * 
     * @param lhs
     *            the first {@link String} collections to be merged.
     * @param rhs
     *            the other {@link String} collections to be merged.
     * 
     * @param <T>
     *            the type of object in the collection to be merged.
     * 
     * @return the new, merge, {@link Collection} instance.
     */
    public static <T> Collection<T> merge(Collection<T> lhs,
                                          Collection<T> rhs) {
        final List<T> result = new ArrayList<T>(lhs);
        for (T item : rhs) {
            if (!result.contains(item)) {
                result.add(item);
            }
        }
        return result;
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
