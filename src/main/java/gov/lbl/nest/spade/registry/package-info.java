/**
 * This package contains the class needed to use and manage file registrations.
 * 
 * @author patton
 */
package gov.lbl.nest.spade.registry;