package gov.lbl.nest.spade.registry;

import jakarta.xml.bind.annotation.XmlEnum;

/**
 * This enumerates the possible priorities for handling bundles.
 * 
 * @author patton
 */
@XmlEnum(String.class)
public enum HandlingPriority {
                              /**
                               * The lowest priority.
                               */
                              LOWEST,
                              /**
                               * A lower priority than low .
                               */
                              LOWER,
                              /**
                               * A lower priority than normal.
                               */
                              LOW,
                              /**
                               * The normal priority.
                               */
                              NORMAL,
                              /**
                               * A higher priority than normal.
                               */
                              HIGH,
                              /**
                               * A higher priority than high.
                               */
                              HIGHER,
                              /**
                               * A highest priority.
                               */
                              HIGHEST,
}