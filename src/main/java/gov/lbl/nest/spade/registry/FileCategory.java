package gov.lbl.nest.spade.registry;

import jakarta.xml.bind.annotation.XmlEnum;

/**
 * This enumerates the possible categories of files.
 * 
 * @author patton
 */
@XmlEnum(String.class)
public enum FileCategory {
                          /**
                           * The metadata file.
                           */
                          METADATA,
                          /**
                           * The raw data file or files.
                           */
                          DATA,
                          /**
                           * The raw data file or files and the metadata is
                           * embedded.
                           */
                          EMBEDDED,
                          /**
                           * The packed file.
                           */
                          PACKED,
                          /**
                           * The compressed file.
                           */
                          COMPRESSED,
                          /**
                           * The transfer semaphore file.
                           */
                          DONE
}