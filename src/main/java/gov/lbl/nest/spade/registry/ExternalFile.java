package gov.lbl.nest.spade.registry;

import java.io.File;

import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.XmlValue;

/**
 * This class provides details of a file external to SPADE.
 * 
 * @author patton
 */
@XmlType
public class ExternalFile {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The String used to separate a directory from a file it contains.
     */
    private static final String DIRECTORY_SEPARATOR = "/";

    // private static member data

    // private instance member data

    /**
     * The location where this file should be delivered.
     */
    private File deliveryLocation;

    /**
     * The {@link ExternalLocation} where this file exists.
     */
    private ExternalLocation location;

    /**
     * The name of the file.
     */
    private String name;

    /**
     * The replacement statement to used to derive the local file name from a
     * external name that is a "regex".
     */
    private String replacement;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected ExternalFile() {
    }

    /**
     * Create a copy of the supplied {@link ExternalFile} instance.
     * 
     * @param rhs
     *            the {@link ExternalFile} instance to be copied.
     */
    public ExternalFile(final ExternalFile rhs) {
        this(rhs.location,
             rhs.name,
             rhs.deliveryLocation,
             rhs.replacement);
    }

    /**
     * Creates an instance of this class.
     * 
     * @param location
     *            the {@link ExternalLocation} where the file may be found.
     * @param name
     *            the name of the file.
     */
    public ExternalFile(final ExternalLocation location,
                        final String name) {
        this(location,
             name,
             null,
             null);
    }

    /**
     * Creates an instance of this class.
     * 
     * @param location
     *            the {@link ExternalLocation} where the file was found.
     * @param name
     *            the name of the file.
     * @param deliveryLocation
     *            the location where this file should be delivered.
     */
    public ExternalFile(final ExternalLocation location,
                        final String name,
                        final File deliveryLocation) {
        this(location,
             name,
             deliveryLocation,
             null);
    }

    /**
     * Creates an instance of this class.
     * 
     * @param location
     *            the {@link ExternalLocation} where the file was found.
     * @param name
     *            the name of the file.
     * @param deliveryLocation
     *            the location where this file should be delivered.
     * @param replacement
     *            the replacement statement to used to derive the local file
     *            name from a external name that is a "regex".
     */
    public ExternalFile(final ExternalLocation location,
                        final String name,
                        final File deliveryLocation,
                        final String replacement) {
        setDeliveryLocation(deliveryLocation);
        setLocation(location);
        this.name = name;
        setReplacement(replacement);
    }

    /**
     * Creates an instance of this class.
     * 
     * @param file
     *            the location of the file.
     */
    public ExternalFile(final File file) {
        setFullPath(file.getPath());
    }

    // instance member method (alphabetic)

    /**
     * Returns the location where this file should be delivered.
     * 
     * @return the location where this file should be delivered.
     */
    @XmlTransient
    public File getDeliveryLocation() {
        return deliveryLocation;
    }

    /**
     * Returns the path to where this file should be delivered.
     * 
     * @return the path to where this file should be delivered.
     */
    @XmlAttribute(name = "delivery")
    protected String getDeliveryPath() {
        if (null == deliveryLocation) {
            return null;
        }
        return deliveryLocation.getPath();
    }

    /**
     * Returns the value of the directory property.
     * 
     * @return the value of the directory property.
     */
    @XmlTransient
    public String getDirectory() {
        return location.getDirectory();
    }

    /**
     * Returns the external location of the file.
     * 
     * @return the external location of the file.
     */
    @XmlValue
    protected String getFullPath() {
        return location.toString() + DIRECTORY_SEPARATOR
               + name;
    }

    /**
     * Returns the value of the host property.
     * 
     * @return the value of the host property.
     */
    @XmlTransient
    public String getHost() {
        return location.getHost();
    }

    /**
     * Returns the {@link ExternalLocation} where this file exists.
     * 
     * @return the {@link ExternalLocation} where this file exists.
     */
    @XmlTransient
    public ExternalLocation getLocation() {
        return location;
    }

    /**
     * Returns the name of the file.
     * 
     * @return the name of the file.
     */
    @XmlTransient
    public String getName() {
        return name;
    }

    /**
     * Returns the replacement statement to used to derive the local file name
     * from a external name that is a "regex".
     * 
     * @return the replacement statement to used to derive the local file name
     *         from a external name that is a "regex".
     */
    @XmlAttribute
    public String getReplacement() {
        return replacement;
    }

    /**
     * Replaces the contents of this object with that of the supplied object.
     * 
     * @param rhs
     *            the object whose contents should be put into this object.
     */
    public void replace(final ExternalFile rhs) {
        setDeliveryLocation(rhs.deliveryLocation);
        setLocation(rhs.location);
        this.name = rhs.name;
        setReplacement(rhs.replacement);
    }

    /**
     * Set the location where this file should be delivered.
     * 
     * @param deliveryLocation
     *            the location where this file should be delivered.
     */
    public void setDeliveryLocation(final File deliveryLocation) {
        this.deliveryLocation = deliveryLocation;
    }

    /**
     * Sets the path to where this file should be delivered.
     * 
     * @param path
     *            the path to where this file should be delivered.
     */
    protected void setDeliveryPath(final String path) {
        if (null == path) {
            deliveryLocation = null;
            return;
        }
        deliveryLocation = new File(path);
    }

    /**
     * Sets the {@link ExternalLocation} where this file exists.
     * 
     * @param location
     *            the {@link ExternalLocation} where this file exists.
     */
    protected void setLocation(final ExternalLocation location) {
        this.location = location;
    }

    /**
     * Sets the replacement statement to used to derive the local file name from
     * a external name that is a "regex".
     * 
     * @param replacement
     *            the replacement statement to used to derive the local file
     *            name from a external name that is a "regex".
     */
    protected void setReplacement(final String replacement) {
        this.replacement = replacement;
    }

    /**
     * Sets the external location of the file.
     * 
     * @param path
     *            the external location of the file.
     */
    protected void setFullPath(final String path) {
        final int index = path.lastIndexOf(DIRECTORY_SEPARATOR);
        if (-1 == index) {
            location = new ExternalLocation(".");
            name = path;
        } else {
            location = new ExternalLocation(path.substring(0,
                                                           index));
            name = path.substring(index + 1);
        }
    }

    // static member methods (alphabetic)

    // Description of this object.
    @Override
    public String toString() {
        return getFullPath();
    }

    // public static void main(String args[]) {}
}
