package gov.lbl.nest.spade.interfaces.metadata;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ServiceLoader;
import java.util.zip.Adler32;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

/**
 * This class creates {@link Checksum} instances for a {@link ChecksumElement}
 * instance.
 * 
 * @author patton
 */
public abstract class ChecksumFactory {

    // public static final member data

    /**
     * The string used to specified that the {@link CRC32} class should be used.
     */
    public final static String CRC32_TYPE = "CRC32";

    /**
     * The string used to specify that the {@link Adler32} class should be used.
     */
    public final static String ADLER32_TYPE = "Adler32";

    // protected static final member data

    // static final member data

    /**
     * The size of chunks with which to calculate the checksum.
     */
    private static final int CHECKSUM_CHUNK = 16384;

    /**
     * The {@link ServiceLoader} instance that will load the
     * {@link ChecksumElement} implementation.
     */
    private final static ServiceLoader<ChecksumFactory> loader = ServiceLoader.load(ChecksumFactory.class,
                                                                                    ChecksumFactory.class.getClassLoader());
    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    /**
     * Returns the {@link Checksum} implementation for the supplied algorithm,
     * or <code>null</code> is none can b created.
     * 
     * @param algorithm
     *            the name of the algorithm used by the returned
     *            {@link Checksum} instance.
     * 
     * @return the created {@link Checksum} instance
     */
    protected abstract Checksum createInstance(final String algorithm);

    // static member methods (alphabetic)

    /**
     * Returns the checksum value for the specified file as calculated by the
     * supplied algorithm.
     * 
     * @param file
     *            the file whose checksum should be calculated.
     * @param algorithm
     *            the algorithm used to calculate the checksum.
     * 
     * @return the checksum for the specified file, or <code>null</code> if the
     *         checksum could not be calculated.
     * 
     * @throws IOException
     *             when the file can not be successfully read.
     */
    public static Long calculateValue(final File file,
                                      final String algorithm) throws IOException {
        Checksum checksum;
        if (null == algorithm) {
            checksum = ChecksumFactory.getChecksum(ChecksumFactory.CRC32_TYPE);
        } else {
            checksum = ChecksumFactory.getChecksum(algorithm);
        }
        if (null == checksum) {
            return null;
        }
        try (FileInputStream in = new FileInputStream(file)) {
            byte[] buffer = new byte[CHECKSUM_CHUNK];
            int count;
            while ((count = in.read(buffer)) > 0) {
                /*
                 * This can throw a NullPointerException when trying to create
                 * the FileInputStream. Should it retry or not?
                 */
                checksum.update(buffer,
                                0,
                                count);
            }
        }

        return checksum.getValue();
    }

    /**
     * Returns an instance of the {@link Checksum} class used to calculate the
     * checksum.
     * 
     * @param algorithm
     *            the name of the algorithm to be used to calculate the
     *            checksum.
     * 
     * @return the {@link Checksum} instance to be used to calculate the
     *         checksum.
     */
    private static Checksum getChecksum(final String algorithm) {
        for (ChecksumFactory checksumAlgorithm : loader) {
            Checksum checksum = checksumAlgorithm.createInstance(algorithm);
            if (checksum != null) {
                return checksum;
            }
        }
        return null;
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    /**
     * Runs the requested algorithm over the specified file.
     * 
     * @param args
     *            filename [algorithm]
     * 
     * @throws IOException
     *             when the file can not be read.
     */
    public static void main(String args[]) throws IOException {
        if (0 == args.length || args.length > 2) {
            System.err.println("One or two arguments required");
            System.exit(1);
        }

        final File file = new File(args[0]);
        if (!file.exists()) {
            System.err.println("File \"" + args[0]
                               + "\" does not exist");
            System.exit(2);
        }

        final String algorithm;
        if (1 == args.length) {
            algorithm = null;
        } else {
            algorithm = args[1];
        }
        final Long value = calculateValue(file,
                                          algorithm);
        if (null == value) {
            System.err.println("Checksum \"" + algorithm
                               + "\" does not exist");
            System.exit(3);
        }

        System.out.println(value.longValue() + " : "
                           + args[0]);
    }
}
