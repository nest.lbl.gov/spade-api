package gov.lbl.nest.spade.interfaces.storage;

import java.io.File;

/**
 * This interface defines how the application manages placing, moving and
 * removing files in, around, and out of tertiary storage.
 * 
 * @author patton
 */
public interface ArchiveManager {

    /**
     * Adds the specified file to the archive.
     * 
     * @param archiveFile
     *            the file to be archived that contains its own metadata.
     * @param placement
     *            the location, within the archive, whether the file should be
     *            placed.
     * @throws ArchiveFailureException
     *             when the file can not be successfully archived.
     */
    void add(File archiveFile,
             File placement) throws ArchiveFailureException;

    /**
     * Adds the specified file along with its data file to the archive as a
     * single file.
     * 
     * @param archiveFile
     *            the file to be archived whose metadata is in the accompanying
     *            file.
     * @param metadataFile
     *            the metadata to be archive with the specified file.
     * @param placement
     *            the location, within the archive, whether the file should be
     *            placed.
     * 
     * @throws ArchiveFailureException
     *             when the file and metadata can not be successfully archived.
     */
    void add(File archiveFile,
             File metadataFile,
             File placement) throws ArchiveFailureException;
}
