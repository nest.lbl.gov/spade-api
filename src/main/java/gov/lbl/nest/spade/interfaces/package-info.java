/**
 * The package contains the Interfaces used by SPADE that can have third party
 * implementations. SPADE itself has default implementations of all these
 * interfaces so it can run with any of these being implemented outside SPADE.
 * 
 * @author patton
 */
package gov.lbl.nest.spade.interfaces;