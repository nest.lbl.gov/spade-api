package gov.lbl.nest.spade.interfaces.metadata;

/**
 * This interface tags classes that contain the metadata for a file.
 * 
 * @author patton
 */
public interface Metadata {
    // No content, just a tagging interface.
}