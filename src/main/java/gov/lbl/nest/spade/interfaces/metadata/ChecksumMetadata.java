package gov.lbl.nest.spade.interfaces.metadata;

/**
 * This interface is used by metadata subclasses that can provide their own
 * checksum.
 * 
 * @author patton
 */
public interface ChecksumMetadata extends
                                  Metadata {

    /**
     * Returns the {@link ChecksumElement} instance of the object.
     * 
     * @return the {@link ChecksumElement} instance of the object.
     */
    ChecksumElement getChecksum();

    /**
     * Sets the {@link ChecksumElement} instance of the object.
     * 
     * @param element
     *            the {@link ChecksumElement} instance of the object.
     */
    void setChecksum(final ChecksumElement element);

}
