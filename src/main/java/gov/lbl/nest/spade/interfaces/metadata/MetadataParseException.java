package gov.lbl.nest.spade.interfaces.metadata;

/**
 * This class is thrown when the is an error parsing a file.
 * 
 * @author patton
 */
public class MetadataParseException extends
                                    Exception {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * Used by Serializable.
     */
    private static final long serialVersionUID = 1L;

    // private static member data

    // private instance member data

    // constructors

    /**
     * Constructs a ParseException with the specified detail message.
     * 
     * @param message
     *            the message describing why this object was thrown.
     */
    public MetadataParseException(String message) {
        super(message);
    }

    /**
     * Constructs a ParseException with the exception that was raised while
     * looking for the instance.
     * 
     * @param cause
     *            the Exception that caused this object was thrown.
     */
    public MetadataParseException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a ParseException with the specified detail message and the
     * exception that was raised while looking for the instance.
     * 
     * @param message
     *            the message describing why this object was thrown.
     * @param cause
     *            the Exception that caused this object was thrown.
     */
    public MetadataParseException(String message,
                                  Throwable cause) {
        super(message,
              cause);
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}

}
