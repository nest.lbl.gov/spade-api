package gov.lbl.nest.spade.interfaces.metadata;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import jakarta.enterprise.inject.Vetoed;

/**
 * This interface defines how standard entities used by SPADE instances are
 * managed.
 * 
 * @author patton
 */
@Vetoed
public interface MetadataManager {

    /**
     * Create a {@link Metadata} instance from the specified file.
     *
     * @param file
     *            the file containing the Metadata content.
     *
     * @return the new {@link Metadata} instance.
     *
     * @throws MetadataParseException
     *             when the specified file is not a valid metadata file.
     * @throws FileNotFoundException
     *             when the specified file does not exist.
     * @throws IOException
     *             when the specified file can not be read.
     */
    Metadata createMetadata(final File file) throws MetadataParseException,
                                             FileNotFoundException,
                                             IOException;

    /**
     * Creates a new {@link Metadata} instance using the defaults, if any, and
     * then adding the specified {@link Metadata} instance.
     *
     * @param metadata
     *            the value to using the the {@link Metadata} instance that
     *            override any defaults.
     * @param defaults
     *            the defaults, if any, to use when creating the
     *            {@link Metadata} instance.
     *
     * @return the created {@link Metadata} instance.
     */
    Metadata createMetadata(final Metadata metadata,
                            final Metadata defaults);

    /**
     * Save the supplied {@link Metadata} instance to the specified
     * {@link File}.
     * 
     * @param metadata
     *            the {@link Metadata} instance to save
     * @param file
     *            the {@link File} in which to save it.
     */
    void save(Metadata metadata,
              File file);
}
