package gov.lbl.nest.spade.interfaces.storage;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * This interface defines how the application manages placing, moving and
 * removing files in, around, and out of the warehouse.
 * 
 * @author patton
 */
public interface WarehouseManager {

    /**
     * The string used to filter the metadata file into a placement request.
     */
    static final String METADATA_FILTER = "metadata";

    /**
     * The string used to filter the metadata file into a placement request.
     */
    static final String DATA_FILTER = "data";

    /**
     * The string used to filter the metadata file into a placement request.
     */
    static final String WRAPPED_FILTER = "wrapped";

    /**
     * The string used to filter the metadata file into a placement request.
     */
    static final String COMPRESSED_FILTER = "compressed";

    /**
     * Copies files from the cache into the warehouse. It is up to the warehouse
     * to decide which files to actually copy.
     * 
     * @param bundle
     *            the name of the bundle of files to be added.
     * @param cachedMeta
     *            the location of the metadata file, if any, in the cache.
     * @param cachedData
     *            the location of the data file in the cache.
     * @param cachedWrapped
     *            the location of the wrapped file, if any, in the cache.
     * @param cachedCompressed
     *            the location of the compressed file, if any, in the cache.
     * @param placedMeta
     *            the location of where the metadata file should be placed in
     *            the warehouse.
     * @param placedData
     *            the location of where the data file should be placed in the
     *            warehouse.
     * @param placedWrapped
     *            the location of where the wrapped file should be placed in the
     *            warehouse.
     * @param placedCompressed
     *            the location of where the compressed file should be placed in
     *            the warehouse.
     * @return the {@link Location} describing the bundle's file in the
     *         warehouse.
     * @throws IOException
     *             when one of the files can not be added.
     * @throws InterruptedException
     *             when the addition to the warehouse is interrupted.
     */
    Location add(final String bundle,
                 final File cachedMeta,
                 final File cachedData,
                 final File cachedWrapped,
                 final File cachedCompressed,
                 final File placedMeta,
                 final File placedData,
                 final File placedWrapped,
                 final File placedCompressed) throws InterruptedException,
                                              IOException;

    /**
     * Deletes the set of files related to the warehouseId.
     * 
     * @param warehouseId
     *            the warehouse id defining the files to be deleted.
     */
    void delete(final String warehouseId);

    /**
     * Returns the {@link Location} of the compressed file, if any, associated
     * by the warehouse id.
     * 
     * @param warehouseId
     *            the warehouse id defining the compressed file to be returned.
     * @return the data file, if any, associated by the warehouse id, otherwise
     *         <code>null</code>.
     */
    Location getCompressed(String warehouseId);

    /**
     * Returns a list of {@link Location} instances for the data file where each
     * entry is associated by the matching supplied warehouse identity.
     * 
     * @param warehouseIds
     *            the list of warehouse ids defining the list of data files to
     *            be returned.
     * @return the list of {@link Location} of the data file where each entry is
     *         associated by the matching supplied warehouse identity.
     */
    List<Location> getData(final List<String> warehouseIds);

    /**
     * Returns the {@link Location} of the data file, if any, associated by the
     * warehouse id.
     * 
     * @param warehouseId
     *            the warehouse id defining the data file to be returned.
     * @return the data file, if any, associated by the warehouse id, otherwise
     *         <code>null</code>.
     */
    Location getData(String warehouseId);

    /**
     * Returns the full {@link Location}, if any, associated by the warehouse
     * id.
     * 
     * @param warehouseId
     *            the warehouse id defining the data file to be returned.
     * @param filters
     *            TODO
     * @return the full Location, if any, associated by the warehouse id.
     */
    Location getLocation(String warehouseId,
                         List<String> filters);

    /**
     * Returns the {@link Location} of the metadata file, if any, associated by
     * the warehouse id.
     * 
     * @param warehouseId
     *            the warehouse id defining the metadata file to be returned.
     * @return the {@link Location} of the metadata file, if any, associated by
     *         the warehouse id.
     */
    Location getMetadata(String warehouseId);

    /**
     * Returns a list of {@link Location} instances that have been modified
     * between the specified dates.
     * 
     * @param after
     *            the time on or after which added files should be include in
     *            the returned list.
     * @param before
     *            the time before which added files should be include in the
     *            returned list.
     * @param maxCount
     *            the maximum number of files to return, 0 is unlimited.
     * @return the list of {@link Location} instances that have been modified
     *         since the specified date.
     */
    List<Location> getModifiedSince(Date after,
                                    Date before,
                                    int maxCount);

    /**
     * Returns a list of {@link Location} instances that have been modified
     * since the specified date.
     * 
     * @param dateTime
     *            the time on or after which added files should be include in
     *            the returned list.
     * @param maxCount
     *            the maximum number of files to return, 0 is unlimited.
     * @return the list of {@link Location} instances that have been modified
     *         since the specified date.
     */
    List<Location> getModifiedSince(Date dateTime,
                                    int maxCount);

    /**
     * Returns the {@link Location} of the wrapped file, if any, associated by
     * the warehouse id.
     * 
     * @param warehouseId
     *            the warehouse id defining the wrapped file to be returned.
     * @return the {@link Location} of the wrapped file, if any, associated by
     *         the warehouse id.
     */
    Location getWrapped(String warehouseId);

    /**
     * Copies the data and metadata files from the cache into the warehouse.
     * 
     * @param bundle
     *            the name of the bundle of files to be moved.
     * @param movedData
     *            the new location of the data file in the warehouse,
     *            <code>null</code> if it is not moved.
     * @param movedMeta
     *            the new location of the metadata file in the warehouse,
     *            <code>null</code> if it is not moved.
     * @param movedWrapped
     *            the new location of the wrapped file in the warehouse,
     *            <code>null</code> if it is not moved.
     * @param movedCompressed
     *            the new location of the compressed file in the warehouse,
     *            <code>null</code> if it is not moved.
     * @return the placement id that to be used by a bookkeeping service to
     *         identify this entry.
     * @throws IOException
     *             when one of the files can not be moved.
     * @throws InterruptedException
     *             when the move within the warehouse is interrupted.
     */
    Object move(final String bundle,
                final File movedData,
                final File movedMeta,
                final File movedWrapped,
                final File movedCompressed) throws InterruptedException,
                                            IOException;
}
