package gov.lbl.nest.spade.interfaces.policy;

import java.io.File;
import java.io.IOException;

/**
 * This interface defines the methods any policy used for packing bundles.
 * 
 * @author patton
 */
public interface PackingPolicy extends
                               ImplementedPolicy {

    /**
     * Returns the suffix to use for wrapped files.
     * 
     * @return the suffix to use for wrapped files.
     */
    String getSuffix();

    /**
     * Packs the supplied data and metadata files into the single packed file.
     * 
     * @param dataFile
     *            the file specifying the location of the data.
     * @param metadataFile
     *            the file containing the metadata.
     * @param isSingle
     *            true if the data payload matching this registration is always
     *            only a single file.
     * @param packedFile
     *            the file into which the packed data should be placed.
     *
     * @throws PolicyFailedException
     *             when the policy can not complete its responsibilities.
     * @throws InterruptedException
     *             when the waiting for space method has been interrupted.
     * @throws IOException
     *             when there is a problem with the I/O.
     */
    void pack(final File dataFile,
              final File metadataFile,
              boolean isSingle,
              final File packedFile) throws PolicyFailedException,
                                     IOException,
                                     InterruptedException;

    /**
     * Unpacks the supplied packed file.
     * 
     * @param packedFile
     *            the file to be unpacked
     * @param directory
     *            the directory into which the file should be unpacked.
     *
     * @throws PolicyFailedException
     *             when the policy can not complete its responsibilities.
     * @throws InterruptedException
     *             when the waiting for space method has been interrupted.
     * @throws IOException
     *             when there is a problem with the I/O.
     */
    void unpack(final File packedFile,
                final File directory) throws PolicyFailedException,
                                      IOException,
                                      InterruptedException;
}
