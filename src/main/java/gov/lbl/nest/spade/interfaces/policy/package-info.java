/**
 * The package contains the Interfaces used by SPADE to implement policy
 * decision for some Tasks.
 * 
 * @author patton
 *
 */
package gov.lbl.nest.spade.interfaces.policy;