package gov.lbl.nest.spade.interfaces.metadata;

import java.util.zip.Adler32;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

/**
 * This class creates the built-in {@link Checksum} subclasses.
 * 
 * @author patton
 */
public class BuiltinChecksums extends
                              ChecksumFactory {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    @Override
    protected Checksum createInstance(String algorithm) {
        if (null == algorithm) {
            return null;
        }
        if (algorithm.equals(ChecksumFactory.CRC32_TYPE)) {
            return new CRC32();
        }
        if (algorithm.equals(ChecksumFactory.ADLER32_TYPE)) {
            return new Adler32();
        }
        return null;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
