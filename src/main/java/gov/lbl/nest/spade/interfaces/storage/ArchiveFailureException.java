package gov.lbl.nest.spade.interfaces.storage;

/**
 * The class is thrown when an {@link ArchiveManager} can not successfully
 * archive its files.
 * 
 * @author patton
 */
public class ArchiveFailureException extends
                                     Exception {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * Used by Serializable.
     */
    private static final long serialVersionUID = 1L;

    // private static member data

    // private instance member data

    // constructors

    /**
     * Creates an instance of this class.
     * 
     * @param message
     *            the detailed message.
     * @param cause
     *            the {@link Throwable} instance that caused this object to be
     *            created.
     */
    public ArchiveFailureException(final String message,
                                   final Throwable cause) {
        super(message,
              cause);
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
