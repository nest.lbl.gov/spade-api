package gov.lbl.nest.spade.interfaces.metadata;

import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlValue;

/**
 * This class implements the checksum element of the {@link ChecksumMetadata}
 * class.
 * 
 * @author patton
 */
public class ChecksumElement {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The algorithm used to calculate the checksum.
     */
    private String algorithm;

    /**
     * The value of the checksum.
     */
    private Long value;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected ChecksumElement() {
        // required for JAXB.
    }

    /**
     * Creates a instance of this class.
     * 
     * @param value
     *            the value of the checksum.
     */
    public ChecksumElement(final Long value) {
        this(value,
             null);
    }

    /**
     * Creates a instance of this class.
     * 
     * @param value
     *            the value of the checksum.
     * @param algorithm
     *            the algorithm used to calculate the checksum.
     */
    public ChecksumElement(final Long value,
                           final String algorithm) {
        this.algorithm = algorithm;
        this.value = value;
    }

    // instance member method (alphabetic)

    /**
     * Returns the algorithm used to calculate the checksum.
     * 
     * @return he algorithm used to calculate the checksum.
     */
    @XmlAttribute
    public String getAlgorithm() {
        return algorithm;
    }

    /**
     * Returns the value of the checksum.
     * 
     * @return the value of the checksum.
     */
    @XmlValue
    public Long getValue() {
        return value;
    }

    /**
     * Sets the algorithm used to calculate the checksum.
     * 
     * @param algorithm
     *            the algorithm used to calculate the checksum.
     */
    protected void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    /**
     * Sets the value of the checksum.
     * 
     * @param value
     *            the value of the checksum.
     */
    protected void setValue(Long value) {
        this.value = value;
    }

    // static member methods (alphabetic)

    // Description of this object.
    @Override
    public String toString() {
        if (null == value) {
            if (null == algorithm) {
                return ("Checksum undefined");
            }
            return ("Checksum not calcuated (algorithm=" + algorithm
                    + ")");
        }
        final StringBuilder sb = new StringBuilder("Checksum=" + value
                                                   + " ");
        if (null == algorithm) {
            sb.append("(unknown algorithm)");
        } else {
            sb.append("(algorithm=" + algorithm
                      + ")");
        }
        return sb.toString();
    }

    // public static void main(String args[]) {}
}
