package gov.lbl.nest.spade.interfaces.storage;

import java.util.List;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 * This class represents a collection of {@link Location} instances.
 * 
 * @author patton
 */
@XmlRootElement
public class Locations {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The list of {@link Location} instances in this object.
     */
    private List<Location> locations;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Locations() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param content
     *            the list of {@link Location} instances in this object.
     */
    public Locations(final List<Location> content) {
        locations = content;
    }

    // instance member method (alphabetic)

    /**
     * Returns the list of {@link Location} instances in this object.
     * 
     * @return the list of {@link Location} instances in this object.
     */
    @XmlElement(name = "location")
    public List<Location> getContent() {
        return locations;
    }

    /**
     * Sets the list of {@link Location} instances in this object.
     * 
     * @param content
     *            the list of {@link Location} instances in this object.
     */
    protected void setContent(List<Location> content) {
        locations = content;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
