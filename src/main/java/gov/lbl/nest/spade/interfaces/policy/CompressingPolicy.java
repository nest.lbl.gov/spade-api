package gov.lbl.nest.spade.interfaces.policy;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import gov.lbl.nest.common.external.ExecutionFailedException;

/**
 * This interface defines the methods any policy used the the SPADE Compressor
 * class needs to implement.
 * 
 * @author patton
 */
public interface CompressingPolicy extends
                                   ImplementedPolicy {

    /**
     * Executes the compression, writing the result to the supplied stream.
     * 
     * @param packedFile
     *            the file to be compressed.
     * @param compressedFile
     *            the {@link FileOutputStream} into which to write the
     *            compressed data.
     * @throws ExecutionFailedException
     *             if the compression could not complete successfully.
     * @throws InterruptedException
     *             when the compression is interrupted.
     * @throws IOException
     *             when there is an IO issue.
     */
    void compress(final File packedFile,
                  final File compressedFile) throws ExecutionFailedException,
                                             InterruptedException,
                                             IOException;

    /**
     * Executes the expansion, writing the result to the supplied stream.
     * 
     * @param compressedFile
     *            the file to be expanded.
     * @param packedFile
     *            the {@link FileOutputStream} into which to write the packed
     *            data.
     * 
     * @throws PolicyFailedException
     *             if the compression could not complete successfully.
     * @throws InterruptedException
     *             when the compression is interrupted.
     * @throws IOException
     *             when there is an IO issue.
     */
    void expand(final File compressedFile,
                final File packedFile) throws PolicyFailedException,
                                       InterruptedException,
                                       IOException;

    /**
     * Returns the suffix used for the compressed file given the packaged
     * filename.
     * 
     * @param fileName
     *            the packaged filename.
     * @return the suffix used for the compressed file given the packaged
     *         filename.
     */
    String getCompressedSuffixFromPackedFile(final String fileName);

    /**
     * Returns the suffix used for the packaged file given the compressed
     * filename.
     * 
     * @param fileName
     *            the compressed filename.
     * @return the suffix used for the packaged file given the compressed
     *         filename.
     */
    String getPackedSuffixFromCompressedFile(final String fileName);
}
