package gov.lbl.nest.spade.interfaces.policy;

import java.io.File;

import gov.lbl.nest.spade.interfaces.metadata.MetadataManager;

/**
 * This interface defines the methods any policy used to place bundles.
 * 
 * @author patton
 */
public interface PlacingPolicy extends
                               ImplementedPolicy {

    /**
     * Returns the placement of the transfer file within the archive.
     * 
     * @param fileName
     *            the normalized name of the file to be archived.
     * @param metadataFile
     *            the cached metadata file.
     * @return the placement of the packaged file within the archive.
     * @throws PolicyFailedException
     *             when the policy can not generate the placement information.
     */
    File getArchivePlacement(final String fileName,
                             final File metadataFile) throws PolicyFailedException;

    /**
     * Returns the placement of the compressed file within the warehouse.
     * 
     * @param fileName
     *            the normalized name of the compressed file.
     * @param metadataFile
     *            the cached metadata file.
     * @return the placement of the compressed file within the warehouse.
     * @throws PolicyFailedException
     *             when the policy can not generate the placement information.
     */
    File getCompressedPlacement(final String fileName,
                                final File metadataFile) throws PolicyFailedException;

    /**
     * Returns the placement of the data file within the warehouse when there
     * are multiple files in the data delivery of the bundle.
     * 
     * @param deliveredDirectory
     *            the directory into which the data files have been delivered.
     * @param bundle
     *            the name of the bundle being placed.
     * @param metadataFile
     *            the cached metadata file.
     * @return the placement of the data file within the warehouse.
     * @throws PolicyFailedException
     *             when the policy can not generate the placement information.
     */
    File getDataPlacement(final File deliveredDirectory,
                          final String bundle,
                          final File metadataFile) throws PolicyFailedException;

    /**
     * Returns the placement of the data file within the warehouse when there is
     * only a single file in the data delivery of the bundle.
     * 
     * @param fileName
     *            the normalized name of data file.
     * @param metadataFile
     *            the cached metadata file.
     * @return the placement of the data file within the warehouse.
     * @throws PolicyFailedException
     *             when the policy can not generate the placement information.
     */
    File getDataPlacement(final String fileName,
                          final File metadataFile) throws PolicyFailedException;

    /**
     * Returns the placement of the metadata file within the warehouse.
     * 
     * @param fileName
     *            the normalized name of the metadata file.
     * @param metadataFile
     *            the cached metadata file.
     * 
     * @return the placement of the metadata file within the warehouse.
     * @throws PolicyFailedException
     *             when the policy can not generate the placement information.
     */
    File getMetadataPlacement(final String fileName,
                              final File metadataFile) throws PolicyFailedException;

    /**
     * Returns the placement of the wrapped file within the warehouse.
     * 
     * @param fileName
     *            the normalized name of wrapped file.
     * @param metadataFile
     *            the cached wrapped file.
     * @return the placement of the wrapped file within the warehouse.
     * @throws PolicyFailedException
     *             when the policy can not generate the placement information.
     */
    File getWrappedPlacement(final String fileName,
                             final File metadataFile) throws PolicyFailedException;

    /**
     * Sets the environment in which this object is executing.
     * 
     * @param configurationDir
     *            the directory where configuration information for this object
     *            may be found.
     * @param manager
     *            the {@link MetadataManager} instance this object should use.
     */
    void setEnvironment(final File configurationDir,
                        final MetadataManager manager);
}
