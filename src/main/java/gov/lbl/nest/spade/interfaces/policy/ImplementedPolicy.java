package gov.lbl.nest.spade.interfaces.policy;

import java.util.Collection;

import gov.lbl.nest.common.configure.InitParam;

/**
 * this interface labels a class as an implementation of a SPADE policy.
 * 
 * @author patton
 */
public interface ImplementedPolicy {
    // No content, just a tagging interface with a helper function.

    /**
     * Returns the value of the named parameter is specified, otherwise
     * <code>null</code>.
     * 
     * @param params
     *            the collection of {@link InitParam} instances to search.
     * @param parameter
     *            the name of the parameter to return.
     * @return the value of the named parameter is specified, otherwise
     *         <code>null</code>.
     */
    public static String getParameter(final Collection<InitParam> params,
                                      final String parameter) {
        if (null != params) {
            for (InitParam param : params) {
                if (parameter.equals(param.getName())) {
                    return param.getValue();
                }
            }
        }
        return null;
    }
}
