package gov.lbl.nest.spade.interfaces.policy;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import gov.lbl.nest.spade.registry.ExternalFile;
import gov.lbl.nest.spade.registry.ExternalLocation;

/**
 * This interface is the {@link ImplementedPolicy} used by the SPADE Fetcher
 * task.
 * 
 * @author patton
 */
public interface FetchingPolicy extends
                                ImplementedPolicy {

    /**
     * Attempts to remove the set of files specified in the collection of
     * {@link ExternalFile} instances.
     * 
     * @param externalFiles
     *            the collection of {@link ExternalFile} instances to be removed.
     * 
     * @return true when all of the specified files no longer exist.
     * 
     * @throws IOException
     *             when a file can not be removed for reasons other than
     *             non-existence.
     * @throws InterruptedException
     *             when the remove is interrupted by another thread or process.
     */
    boolean delete(final Collection<ExternalFile> externalFiles) throws IOException,
                                                                 InterruptedException;

    /**
     * Attempts to remove the file specified by the {@link ExternalFile} instance.
     * 
     * @param externalFile
     *            the {@link ExternalFile} instances to be removed.
     * 
     * @return true when all of the specified file no longer exist.
     * 
     * @throws IOException
     *             when the file can not be removed for reasons other than
     *             non-existence.
     * @throws InterruptedException
     *             when the remove is interrupted by another thread or process.
     */
    boolean delete(final ExternalFile externalFile) throws IOException,
                                                    InterruptedException;

    /**
     * Returns the time when the file specified by the {@link ExternalFile} instance
     * was last modified.
     * 
     * @param externalFile
     *            the {@link ExternalFile} instance whose last modified time should
     *            be returned.
     * 
     * @return the time when the file specified by the {@link ExternalFile} instance
     *         was last modified.
     */
    long getLastModified(ExternalFile externalFile);

    /**
     * Returns the paths to all of the sub-directories under the specified
     * {@link ExternalLocation} instance.
     * 
     * @param root
     *            the {@link ExternalLocation} instance specifying the directory
     *            whose sub-directories should be returned.
     * 
     * @return the paths to all of the sub-directories under the specified
     *         {@link ExternalLocation} instance.
     */
    List<String> getSubdirs(ExternalLocation root);

    /**
     * Lists the contents of the directory specified by the ExternalFile instance.
     * 
     * @param location
     *            the {@link ExternalFile} whose contents should be returned.
     * @param filter
     *            the {@link FilenameFilter} instance to select the file name to be
     *            returned.
     * 
     * @return the collection of {@link String} instances of files whose names
     *         matched the supplied {@link FilenameFilter} instance.
     */
    List<String> list(ExternalLocation location,
                      FilenameFilter filter);

    /**
     * Receives a file from an 'external' location by copying it into the specified
     * "internal" location.
     * 
     * @param external
     *            the {@link ExternalFile} describing the file to be copied.
     * @param internal
     *            the {@link File} describing the destination of the copy.
     * 
     * @return The {@link ExternalFile} in which the delivery location, in the case
     *         where the supplied {@link ExternalFile} has a replacement property,
     *         is resolved to that actual file.
     * 
     * @throws IOException
     *             when the file can not be copied
     * @throws InterruptedException
     *             when the copy is interrupted by another thread or process.
     */
    ExternalFile receive(final ExternalFile external,
                         final File internal) throws IOException,
                                              InterruptedException;

    /**
     * Receives a file from an 'external' location by copying it into the specified
     * "internal" location.
     * 
     * @param external
     *            the {@link ExternalFile} describing the file to be copied.
     * @param internal
     *            the {@link File} describing the destination of the copy.
     * @param softlink
     *            true if this method may use a soft-link to receive the data. In
     *            this case when it is removed from the cache only the soft-link
     *            will be removed and management of the file is handled externally.
     * 
     * @return The {@link ExternalFile} in which the delivery location, in the case
     *         where the supplied {@link ExternalFile} has a replacement property,
     *         is resolved to that actual file.
     * 
     * @throws IOException
     *             when the file can not be copied
     * @throws InterruptedException
     *             when the copy is interrupted by another thread or process.
     */
    ExternalFile receive(final ExternalFile external,
                         final File internal,
                         final boolean softlink) throws IOException,
                                                 InterruptedException;

    /**
     * Receives a file from an 'external' location by copying it into the specified
     * "internal" location.
     * 
     * @param external
     *            the {@link ExternalFile} describing the file to be copied.
     * @param internal
     *            the {@link File} describing the destination of the copy.
     * @param softlink
     *            true if this method may use a soft-link to receive the data. In
     *            this case when it is removed from the cache only the soft-link
     *            will be removed and management of the file is handled externally.
     * @param hardlink
     *            true if this method may use a hard-link to receive the data.
     * 
     * @return The {@link ExternalFile} in which the delivery location, in the case
     *         where the supplied {@link ExternalFile} has a replacement property,
     *         is resolved to that actual file.
     * 
     * @throws IOException
     *             when the file can not be copied
     * @throws InterruptedException
     *             when the copy is interrupted by another thread or process.
     */
    ExternalFile receive(final ExternalFile external,
                         final File internal,
                         final boolean softlink,
                         final boolean hardlink) throws IOException,
                                                 InterruptedException;

}
