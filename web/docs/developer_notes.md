# Developer Notes for `spade-api` #

These notes are to help developers develop, maintain and debug the `spade-api` Java library.


# Index of Notes #

This document is a list of notes. They are in no particular order (as defining one and maintaining it does not seem the best use of time).

The current topic covered are:

*   [Building `spade-api`](#building-spade-api)
*   [Deploying `spade-api`](#deploying-spade-api)


# Building `spade-api` #

The following assumes [gradle](https://gradle.org/) is already installed and shows how to build and install a released version of `spade-api` locally.

    SPADE-API_VERSION=1.7.12
    git clone git@gitlab.com:nest.lbl.gov/spade-api.git
    cd spade-api
    git checkout ${SPADE-API_VERSION}
    ./gradlew clean build publishMavenPublicationToMavenLocal

Clearly to build the `master` the `SPADE-API_VERSION` should be set to that value.

The `publishMavenPublicationToMavenLocal` option of the `gradle` command means that the resulting JAR file is put in the local Maven repository so that you can build against this version without have to do a full deployment.

---
*Note:* This step assumes all of the JAR files on which `spade-api` depends are available from their maven repositories, if not the following links show how to build them.

*   [`nest-common`](https://gitlab.com/nest.lbl.gov/nest-common/-/blob/master/developer_notes.md#building-nest-common)
*   [`nest-jee`](https://gitlab.com/nest.lbl.gov/nest-jee/-/blob/master/developer_notes.md#building-nest-jee)

---


# Deploying `spade-api` #

As noted in the [Building `spade-api`](#building-spade-api) section, you can create a local copy of the JAR file in order to build against it. When you are ready to deploy a released version of the project all you need to do is tag the code and push that tag to the [GitLab respoistory](https://gitlab.com/nest.lbl.gov/spade-api). Provided the tag is of the form `XX.YY.ZZ.aaa`, where `XX`, `YY` and `ZZ` are numerical and `aaa` should be something along the lines of `devA`, the [CI/CD](https://gitlab.com/nest.lbl.gov/spade-api/-/pipelines) will be triggered an, if successful, will deploy the resulting artifact to the appropriate [repository](https://gitlab.com/groups/nest.lbl.gov/-/packages).

---
*Note:* Access to the package repository on GitLab require a deploy-token. Rather than needing every use to acquire one, one has already been set of the web server at `http://nest.lbl.gov/`, and the following URL can be used to replace the one provide by the GitLab repository.

*   [https://nest.lbl.gov/maven2/repository](https://nest.lbl.gov/maven2/repository)

---
